package com.shrinivasagro.dto;

public class StockDto {


	private int stockId;
	private int itemId;
	private int itemTypeId;
	private int itemCompanyId;
	private int sizeId;
	private double currentStock;
	

	private String itemName;
	private String typeName;
	private String itemCompanyName;
	private String itemSize;
	private String inout;
	public int getStockId() {
		return stockId;
	}
	public void setStockId(int stockId) {
		this.stockId = stockId;
	}
	public int getItemId() {
		return itemId;
	}
	public void setItemId(int itemId) {
		this.itemId = itemId;
	}
	public int getItemTypeId() {
		return itemTypeId;
	}
	public void setItemTypeId(int itemTypeId) {
		this.itemTypeId = itemTypeId;
	}
	public int getItemCompanyId() {
		return itemCompanyId;
	}
	public void setItemCompanyId(int itemCompanyId) {
		this.itemCompanyId = itemCompanyId;
	}
	public int getSizeId() {
		return sizeId;
	}
	public void setSizeId(int sizeId) {
		this.sizeId = sizeId;
	}
	public double getCurrentStock() {
		return currentStock;
	}
	public void setCurrentStock(double currentStock) {
		this.currentStock = currentStock;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public String getTypeName() {
		return typeName;
	}
	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}
	public String getItemCompanyName() {
		return itemCompanyName;
	}
	public void setItemCompanyName(String itemCompanyName) {
		this.itemCompanyName = itemCompanyName;
	}
	public String getItemSize() {
		return itemSize;
	}
	public void setItemSize(String itemSize) {
		this.itemSize = itemSize;
	}
	public String getInout() {
		return inout;
	}
	public void setInout(String inout) {
		this.inout = inout;
	}
	
}
