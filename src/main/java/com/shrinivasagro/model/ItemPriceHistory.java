package com.shrinivasagro.model;

import java.util.Date;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

public class ItemPriceHistory {

	@Id 
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	private int itemPriceId;
	private int itemId;
	private double itemPrice;
	private Date createdDate;
	private int createdBy;
	
	public ItemPriceHistory() {}

	public ItemPriceHistory(int id, int itemPriceId, int itemId, double itemPrice, Date createdDate, int createdBy) {
		super();
		this.id = id;
		this.itemPriceId = itemPriceId;
		this.itemId = itemId;
		this.itemPrice = itemPrice;
		this.createdDate = createdDate;
		this.createdBy = createdBy;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getItemPriceId() {
		return itemPriceId;
	}

	public void setItemPriceId(int itemPriceId) {
		this.itemPriceId = itemPriceId;
	}

	public int getItemId() {
		return itemId;
	}

	public void setItemId(int itemId) {
		this.itemId = itemId;
	}

	public double getItemPrice() {
		return itemPrice;
	}

	public void setItemPrice(double itemPrice) {
		this.itemPrice = itemPrice;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public int getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(int createdBy) {
		this.createdBy = createdBy;
	}

	
}
