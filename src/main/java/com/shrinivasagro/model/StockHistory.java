package com.shrinivasagro.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class StockHistory {

	@Id
	@GeneratedValue
	private int id;
	private int itemId;
	private int itemTypeId;
	private int itemCompanyId;
	private int sizeId;
	private double stockValue;
	private int credit_DebitStatus;   // If credit =1,  debit = 2
	private int createBy;
	private Date createDate;
	
	public StockHistory() {}

	public StockHistory(int id, int itemId, int itemTypeId, int itemCompanyId, int sizeId, double stockValue,
			int credit_DebitStatus, int createBy, Date createDate) {
		super();
		this.id = id;
		this.itemId = itemId;
		this.itemTypeId = itemTypeId;
		this.itemCompanyId = itemCompanyId;
		this.sizeId = sizeId;
		this.stockValue = stockValue;
		this.credit_DebitStatus = credit_DebitStatus;
		this.createBy = createBy;
		this.createDate = createDate;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getItemId() {
		return itemId;
	}

	public void setItemId(int itemId) {
		this.itemId = itemId;
	}

	public int getItemTypeId() {
		return itemTypeId;
	}

	public void setItemTypeId(int itemTypeId) {
		this.itemTypeId = itemTypeId;
	}

	public int getItemCompanyId() {
		return itemCompanyId;
	}

	public void setItemCompanyId(int itemCompanyId) {
		this.itemCompanyId = itemCompanyId;
	}

	public int getSizeId() {
		return sizeId;
	}

	public void setSizeId(int sizeId) {
		this.sizeId = sizeId;
	}

	public double getStockValue() {
		return stockValue;
	}

	public void setStockValue(double stockValue) {
		this.stockValue = stockValue;
	}

	public int getCredit_DebitStatus() {
		return credit_DebitStatus;
	}

	public void setCredit_DebitStatus(int credit_DebitStatus) {
		this.credit_DebitStatus = credit_DebitStatus;
	}

	public int getCreateBy() {
		return createBy;
	}

	public void setCreateBy(int createBy) {
		this.createBy = createBy;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	
	
	
}
