package com.shrinivasagro.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class ItemPrice {

	@Id 
	@GeneratedValue
	private int id;
	private int itemId;
	private int itemTypeId;
	private int itemCompanyId;
	private int sizeId;
	private double itemInitialPrice;
	private double itemInitialDiscount;
	private String hsnCode;
	private int itemUnitId;
	private String createdDate;
	private String updateDate;
	private int itemStatus;

	private String itemName;
	
	public ItemPrice(int id, int itemId, int itemTypeId, int itemCompanyId, int sizeId, double itemInitialPrice,
			double itemInitialDiscount, String hsnCode, int itemUnitId, String createdDate, String updateDate,
			int itemStatus, String itemName) {
		super();
		this.id = id;
		this.itemId = itemId;
		this.itemTypeId = itemTypeId;
		this.itemCompanyId = itemCompanyId;
		this.sizeId = sizeId;
		this.itemInitialPrice = itemInitialPrice;
		this.itemInitialDiscount = itemInitialDiscount;
		this.hsnCode = hsnCode;
		this.itemUnitId = itemUnitId;
		this.createdDate = createdDate;
		this.updateDate = updateDate;
		this.itemStatus = itemStatus;
		this.itemName = itemName;
	}

	public ItemPrice() {}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getItemId() {
		return itemId;
	}

	public void setItemId(int itemId) {
		this.itemId = itemId;
	}

	public int getItemTypeId() {
		return itemTypeId;
	}

	public void setItemTypeId(int itemTypeId) {
		this.itemTypeId = itemTypeId;
	}

	public int getItemCompanyId() {
		return itemCompanyId;
	}

	public void setItemCompanyId(int itemCompanyId) {
		this.itemCompanyId = itemCompanyId;
	}

	public int getSizeId() {
		return sizeId;
	}

	public void setSizeId(int sizeId) {
		this.sizeId = sizeId;
	}

	public double getItemInitialPrice() {
		return itemInitialPrice;
	}

	public void setItemInitialPrice(double itemInitialPrice) {
		this.itemInitialPrice = itemInitialPrice;
	}

	public double getItemInitialDiscount() {
		return itemInitialDiscount;
	}

	public void setItemInitialDiscount(double itemInitialDiscount) {
		this.itemInitialDiscount = itemInitialDiscount;
	}

	public String getHsnCode() {
		return hsnCode;
	}

	public void setHsnCode(String hsnCode) {
		this.hsnCode = hsnCode;
	}

	public int getItemUnitId() {
		return itemUnitId;
	}

	public void setItemUnitId(int itemUnitId) {
		this.itemUnitId = itemUnitId;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public String getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}

	public int getItemStatus() {
		return itemStatus;
	}

	public void setItemStatus(int itemStatus) {
		this.itemStatus = itemStatus;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}


}
