package com.shrinivasagro.model;

import javax.persistence.Id;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
public class ItemType {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	private String typeName;
    private double gstPer;
	
	public ItemType() {}
	public ItemType(int id, String typeName, double gstPer) {
		super();
		this.id = id;
		this.typeName = typeName;
		this.gstPer = gstPer;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	public double getGstPer() {
		return gstPer;
	}

	public void setGstPer(double gstPer) {
		this.gstPer = gstPer;
	}
	
}
