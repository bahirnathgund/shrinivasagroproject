package com.shrinivasagro.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class ItemSize {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	private String itemSize;
	
	public ItemSize()
	{}

	public ItemSize(int id, String itemSize) {
		super();
		this.id = id;
		this.itemSize = itemSize;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getItemSize() {
		return itemSize;
	}

	public void setItemSize(String itemSize) {
		this.itemSize = itemSize;
	}
	
	
}
