package com.shrinivasagro.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class ItemUnit {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	private String itemUnit;
	
	public ItemUnit() {}

	public ItemUnit(int id, String itemUnit) {
		super();
		this.id = id;
		this.itemUnit = itemUnit;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getItemUnit() {
		return itemUnit;
	}

	public void setItemUnit(String itemUnit) {
		this.itemUnit = itemUnit;
	}
	
}
