package com.shrinivasagro.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
public class ItemCompany {

	@Id 
	@GeneratedValue
	private int id;
	private String itemCompanyName;
	private int itemTypeId;
	
	public ItemCompany() {}
	
	public ItemCompany(int id, String itemCompanyName, int itemTypeId) {
		super();
		this.id = id;
		this.itemCompanyName = itemCompanyName;
		this.itemTypeId = itemTypeId;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getItemCompanyName() {
		return itemCompanyName;
	}

	public void setItemCompanyName(String itemCompanyName) {
		this.itemCompanyName = itemCompanyName;
	}

	public int getItemTypeId() {
		return itemTypeId;
	}

	public void setItemTypeId(int itemTypeId) {
		this.itemTypeId = itemTypeId;
	}
	
	
}
