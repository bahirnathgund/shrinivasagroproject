package com.shrinivasagro.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Stock {

	@Id
	@GeneratedValue
	private int id;
	private int itemId;
	private int itemTypeId;
	private int itemCompanyId;
	private int sizeId;
	private double currentStock;
	
	public Stock() {}

	public Stock(int id, int itemId, int itemTypeId, int itemCompanyId, int sizeId, double currentStock) {
		super();
		this.id = id;
		this.itemId = itemId;
		this.itemTypeId = itemTypeId;
		this.itemCompanyId = itemCompanyId;
		this.sizeId = sizeId;
		this.currentStock = currentStock;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getItemId() {
		return itemId;
	}

	public void setItemId(int itemId) {
		this.itemId = itemId;
	}

	public int getItemTypeId() {
		return itemTypeId;
	}

	public void setItemTypeId(int itemTypeId) {
		this.itemTypeId = itemTypeId;
	}

	public int getItemCompanyId() {
		return itemCompanyId;
	}

	public void setItemCompanyId(int itemCompanyId) {
		this.itemCompanyId = itemCompanyId;
	}

	public int getSizeId() {
		return sizeId;
	}

	public void setSizeId(int sizeId) {
		this.sizeId = sizeId;
	}

	public double getCurrentStock() {
		return currentStock;
	}

	public void setCurrentStock(double currentStock) {
		this.currentStock = currentStock;
	}
	
}
