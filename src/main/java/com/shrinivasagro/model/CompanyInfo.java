package com.shrinivasagro.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="companyinfo")
public class CompanyInfo {

	@Id
	@GeneratedValue
	private int id;
	private String companyName;
	private String companyAddress1;
	private String companyAddress2;
	private String gstNumber;
	private String tanNumber;
	
	public CompanyInfo() {}
	
	public CompanyInfo(int id, String companyName, String companyAddress1, String companyAddress2, String gstNumber,
			String tanNumber) {
		super();
		this.id = id;
		this.companyName = companyName;
		this.companyAddress1 = companyAddress1;
		this.companyAddress2 = companyAddress2;
		this.gstNumber = gstNumber;
		this.tanNumber = tanNumber;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getCompanyAddress1() {
		return companyAddress1;
	}
	public void setCompanyAddress1(String companyAddress1) {
		this.companyAddress1 = companyAddress1;
	}
	public String getCompanyAddress2() {
		return companyAddress2;
	}
	public void setCompanyAddress2(String companyAddress2) {
		this.companyAddress2 = companyAddress2;
	}
	public String getGstNumber() {
		return gstNumber;
	}
	public void setGstNumber(String gstNumber) {
		this.gstNumber = gstNumber;
	}
	public String getTanNumber() {
		return tanNumber;
	}
	public void setTanNumber(String tanNumber) {
		this.tanNumber = tanNumber;
	}
	
	
}
