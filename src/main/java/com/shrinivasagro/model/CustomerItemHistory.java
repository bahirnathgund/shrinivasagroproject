package com.shrinivasagro.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class CustomerItemHistory {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	private int customerId;
	private int itemId;
	private int itemTypeId;
	private int itemCompanyId;
	private int sizeId;
	private int itemUnitId;
	private double itemQty;
	private double itemPrice;
	private double itemdiscount;
	private double gstAmount;
	private double totalAmount;
	
	public CustomerItemHistory() {}

	public CustomerItemHistory(int id, int customerId, int itemId, int itemTypeId, int itemCompanyId, int sizeId,
			int itemUnitId, double itemQty, double itemPrice, double itemdiscount, double gstAmount,
			double totalAmount) {
		super();
		this.id = id;
		this.customerId = customerId;
		this.itemId = itemId;
		this.itemTypeId = itemTypeId;
		this.itemCompanyId = itemCompanyId;
		this.sizeId = sizeId;
		this.itemUnitId = itemUnitId;
		this.itemQty = itemQty;
		this.itemPrice = itemPrice;
		this.itemdiscount = itemdiscount;
		this.gstAmount = gstAmount;
		this.totalAmount = totalAmount;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getCustomerId() {
		return customerId;
	}

	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}

	public int getItemId() {
		return itemId;
	}

	public void setItemId(int itemId) {
		this.itemId = itemId;
	}

	public int getItemTypeId() {
		return itemTypeId;
	}

	public void setItemTypeId(int itemTypeId) {
		this.itemTypeId = itemTypeId;
	}

	public int getItemCompanyId() {
		return itemCompanyId;
	}

	public void setItemCompanyId(int itemCompanyId) {
		this.itemCompanyId = itemCompanyId;
	}

	public int getSizeId() {
		return sizeId;
	}

	public void setSizeId(int sizeId) {
		this.sizeId = sizeId;
	}

	public int getItemUnitId() {
		return itemUnitId;
	}

	public void setItemUnitId(int itemUnitId) {
		this.itemUnitId = itemUnitId;
	}

	public double getItemQty() {
		return itemQty;
	}

	public void setItemQty(double itemQty) {
		this.itemQty = itemQty;
	}

	public double getItemPrice() {
		return itemPrice;
	}

	public void setItemPrice(double itemPrice) {
		this.itemPrice = itemPrice;
	}

	public double getItemdiscount() {
		return itemdiscount;
	}

	public void setItemdiscount(double itemdiscount) {
		this.itemdiscount = itemdiscount;
	}

	public double getGstAmount() {
		return gstAmount;
	}

	public void setGstAmount(double gstAmount) {
		this.gstAmount = gstAmount;
	}

	public double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}
	
	
	
}
