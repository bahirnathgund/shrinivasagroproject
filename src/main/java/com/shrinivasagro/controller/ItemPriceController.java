package com.shrinivasagro.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.shrinivasagro.model.Item;
import com.shrinivasagro.model.ItemCompany;
import com.shrinivasagro.model.ItemPrice;
import com.shrinivasagro.model.ItemSize;
import com.shrinivasagro.model.ItemType;
import com.shrinivasagro.model.ItemUnit;
import com.shrinivasagro.services.ItemPriceService;
import com.shrinivasagro.services.ItemService;

@Controller
public class ItemPriceController {


	@Autowired
	ItemPriceService itemPriceService;

	@Autowired
	ItemService itemService;
	
	@RequestMapping("/ItemPriceMaster")
	public String ItemTypeMaster(Model model, HttpServletRequest req,HttpServletResponse res)
	{
		List<ItemPrice> itemPriceList=itemPriceService.getItemPriceList();
		model.addAttribute("itemPriceList",itemPriceList);
		return "ItemPriceMaster";
	}

	@RequestMapping("/AddItemPrice")
	public String AddItemPrice(Model model, HttpServletRequest req,HttpServletResponse res)
	{
		List<ItemType> itemTypeList=itemService.getItemTypeList();
		List<ItemSize> itemSizeList= itemService.getItemSizeList();
		List<ItemUnit> itemUnitList= itemService.getItemUnitList();
		
		model.addAttribute("itemUnitList",itemUnitList);
		model.addAttribute("itemSizeList",itemSizeList);
		model.addAttribute("itemTypeList",itemTypeList);
		return "AddItemPrice";
	}

	@RequestMapping(value="/AddItemPrice", method=RequestMethod.POST)
	public String AddItemPrice(@ModelAttribute ItemPrice itemPrice, Model model, HttpServletRequest req,HttpServletResponse res)
	{
		int result =itemPriceService.saveiItemPrice(itemPrice);



		List<ItemType> itemTypeList=itemService.getItemTypeList();
		List<ItemSize> itemSizeList= itemService.getItemSizeList();
		List<ItemUnit> itemUnitList= itemService.getItemUnitList();
		
		model.addAttribute("itemUnitList",itemUnitList);
		model.addAttribute("itemSizeList",itemSizeList);
		model.addAttribute("itemTypeList",itemTypeList);
		return "AddItemPrice";
	}


	@ResponseBody
	@RequestMapping("/getItemCompamyListByItemType")
	public List<ItemCompany> getItemCompamyListByItemType(@RequestParam("itemTypeId") int itemTypeId, HttpSession session)
	{
		try
		{

			List<ItemCompany> itemCompanyList=itemService.getItemCompamyListByItemType(itemTypeId);
			
			return itemCompanyList;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}

	}	
	
	@ResponseBody
	@RequestMapping("/getItemListByItemType")
	public List<Item> getItemListByItemType(@RequestParam("itemTypeId") int itemTypeId, HttpSession session)
	{
		try
		{

			List<Item> itemList=itemService.getItemListByItemType(itemTypeId);
			return itemList;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}

	}	
}
