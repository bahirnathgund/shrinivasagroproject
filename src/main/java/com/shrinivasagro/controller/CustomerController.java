package com.shrinivasagro.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.shrinivasagro.model.Customer;
import com.shrinivasagro.model.CustomerItemHistory;
import com.shrinivasagro.model.Item;
import com.shrinivasagro.model.ItemPrice;
import com.shrinivasagro.model.ItemSize;
import com.shrinivasagro.model.ItemType;
import com.shrinivasagro.model.ItemUnit;
import com.shrinivasagro.model.Stock;
import com.shrinivasagro.model.StockHistory;
import com.shrinivasagro.services.CustomerService;
import com.shrinivasagro.services.ItemPriceService;
import com.shrinivasagro.services.ItemService;
import com.shrinivasagro.services.StockService;

@Controller
public class CustomerController {

	@Autowired
	CustomerService customerService;

	@Autowired
	ItemPriceService itemPriceService;

	@Autowired
	ItemService itemService;

	@Autowired
	StockService stockService;
	
	@RequestMapping("/CustomerMaster")
	public String CustomerMaster(Model model, HttpServletRequest req,HttpServletResponse res)
	{
		List<Customer> customerist=customerService.getCustomerList();

		model.addAttribute("customerist",customerist);
		return "CustomerMaster";
	}

	@RequestMapping("/AddCustomer")
	public String AddCustomer(Model model, HttpServletRequest req,HttpServletResponse res)
	{

		List<ItemType> itemTypeList=itemService.getItemTypeList();
		List<ItemSize> itemSizeList= itemService.getItemSizeList();
		List<ItemUnit> itemUnitList= itemService.getItemUnitList();

		model.addAttribute("itemUnitList",itemUnitList);
		model.addAttribute("itemSizeList",itemSizeList);
		model.addAttribute("itemTypeList",itemTypeList);
		return "AddCustomer";
	}

	@RequestMapping(value="/AddCustomer", method=RequestMethod.POST)
	public String AddCustomer(@ModelAttribute Customer customer, Model model, HttpServletRequest req,HttpServletResponse res)
	{


		String itemIds=req.getParameter("itemIds");
		String itemTypeIds=req.getParameter("itemTypeIds");
		String itemCompanyIds=req.getParameter("itemCompanyIds").trim();
		String sizeIds=req.getParameter("sizeIds");
		String itemUnitIds=req.getParameter("itemUnitIds");
		String itemQtys=req.getParameter("itemQtys");
		String itemPrices=req.getParameter("itemPrices");
		String itemdiscounts=req.getParameter("itemdiscounts");
		String gstAmounts=req.getParameter("gstAmounts");
		String totalAmounts=req.getParameter("totalAmounts");
		String[] itemIdsArray = itemIds.split("###!###");
		String[] itemTypeIdsArray = itemTypeIds.split("###!###");
		String[] itemCompanyIdsArray = itemCompanyIds.split("###!###");
		String[] sizeIdsArray = sizeIds.split("###!###");
		String[] itemUnitIdsArray = itemUnitIds.split("###!###");
		String[] itemQtysArray = itemQtys.split("###!###");
		String[] itemPricesArray = itemPrices.split("###!###");
		String[] itemdiscountsArray = itemdiscounts.split("###!###");
		String[] gstAmountsArray = gstAmounts.split("###!###");
		String[] totalAmountsArray = totalAmounts.split("###!###");


		Customer Customer1 =customerService.saveCustomer(customer);
		
		CustomerItemHistory customerItemHistory=new CustomerItemHistory();
		Stock stock=new Stock();
		StockHistory stockHistory=new StockHistory();
		
		for(int i=0;i<itemIdsArray.length;i++)
		{
			try {
				customerItemHistory=new CustomerItemHistory();
				customerItemHistory.setCustomerId(Customer1.getId());
				customerItemHistory.setItemId(Integer.parseInt(itemIdsArray[i]));
				customerItemHistory.setItemTypeId(Integer.parseInt(itemTypeIdsArray[i]));
				customerItemHistory.setItemCompanyId(Integer.parseInt(itemCompanyIdsArray[i]));
				customerItemHistory.setSizeId(Integer.parseInt(sizeIdsArray[i]));
				customerItemHistory.setItemUnitId(Integer.parseInt(itemUnitIdsArray[i]));
				customerItemHistory.setItemQty(Double.parseDouble(itemQtysArray[i]));
				customerItemHistory.setItemPrice(Double.parseDouble(itemPricesArray[i]));
				customerItemHistory.setItemdiscount(0);
				customerItemHistory.setGstAmount(0);
				customerItemHistory.setTotalAmount(Double.parseDouble(totalAmountsArray[i]));

				customerService.saveCustomerItemList(customerItemHistory);
				
				stockService.deductItemFromStock(customerItemHistory);
				stockService.addStockHistory(customerItemHistory);
				
			}catch (Exception e) {
				e.printStackTrace();
				// TODO: handle exception
			}

		}

		
		List<Customer> customerist=customerService.getCustomerList();

		model.addAttribute("customerist",customerist);
		return "CustomerMaster";
	}

	@ResponseBody
	@RequestMapping("/getItemPriceDetails")
	public ItemPrice getItemPriceDetails(@RequestParam("itemTypeId") int itemTypeId, @RequestParam("itemCompanyId") int itemCompanyId, @RequestParam("itemId") int itemId, @RequestParam("sizeId") int sizeId, @RequestParam("itemUnitId") int itemUnitId, HttpSession session)
	{
		try
		{

			ItemPrice itemPrice = itemPriceService.getItemPriceDetails(itemTypeId,itemCompanyId,itemId,sizeId,itemUnitId);
			return itemPrice;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}

	}


	@ResponseBody
	@RequestMapping("/addCustomerItem")
	public List<Item> addCustomerItem(@RequestParam("itemTypeId") int itemTypeId, @RequestParam("itemCompanyId") int itemCompanyId, @RequestParam("itemId") int itemId, 
			@RequestParam("sizeId") int sizeId, @RequestParam("itemUnitId") int itemUnitId,@RequestParam("itemInitialPrice") double itemInitialPrice, @RequestParam("itemQty") double itemQty,@RequestParam("gstAmount") double gstAmount,@RequestParam("totalAmount") double totalAmount, HttpSession session)
	{
		//CustomerItemHistory
		try
		{
			customerService.addCustomerItem(itemTypeId, itemCompanyId , itemId, sizeId, itemUnitId , itemInitialPrice, itemQty,gstAmount,totalAmount);
			List<Item> itemList=itemService.getItemListByItemType(itemTypeId);
			return itemList;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}

	}	
}
