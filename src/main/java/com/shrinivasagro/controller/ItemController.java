package com.shrinivasagro.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.shrinivasagro.model.Item;
import com.shrinivasagro.model.ItemCompany;
import com.shrinivasagro.model.ItemType;
import com.shrinivasagro.services.ItemService;

@Controller
public class ItemController {

	@Autowired
	ItemService itemService;

	@RequestMapping("/ItemTypeMaster")
	public String ItemTypeMaster(Model model, HttpServletRequest req,HttpServletResponse res)
	{
		List<ItemType> itemTypeList=itemService.getItemTypeList();

		model.addAttribute("itemTypeList",itemTypeList);
		return "ItemTypeMaster";
	}

	@RequestMapping("/ItemCompanyMaster")
	public String ItemCompanyMaster(Model model, HttpServletRequest req,HttpServletResponse res)
	{
		List<ItemCompany> itemCompanyList=itemService.getItemCompanyList();
		model.addAttribute("itemCompanyList",itemCompanyList);
		return "ItemCompanyMaster";
	}

	@RequestMapping("/ItemMaster")
	public String itemMaster(Model model, HttpServletRequest req,HttpServletResponse res)
	{
		List<Item> itemList=itemService.getItemList();
		List<ItemType> itemTypeList=itemService.getItemTypeList();

		model.addAttribute("itemTypeList",itemTypeList);
		model.addAttribute("itemList",itemList);
		return "ItemMaster";
	}


	@RequestMapping("/AddItemType")
	public String addItemType(Model model, HttpServletRequest req,HttpServletResponse res)
	{
	
		return "AddItemType";
	}

	@RequestMapping(value="/AddItemType", method=RequestMethod.POST)
	public String addItemType(@ModelAttribute ItemType itemType, Model model, HttpServletRequest req,HttpServletResponse res)
	{
		int result =itemService.saveItemType(itemType);
		return "AddItemType";
	}

	@RequestMapping("/AddItemCompany")
	public String AddItemCompany(Model model, HttpServletRequest req,HttpServletResponse res)
	{
		List<ItemType> itemTypeList=itemService.getItemTypeList();

		model.addAttribute("itemTypeList",itemTypeList);
		return "AddItemCompany";
	}

	@RequestMapping(value="/AddItemCompany", method=RequestMethod.POST)
	public String AddItemCompany(@ModelAttribute ItemCompany itemCompany, Model model, HttpServletRequest req,HttpServletResponse res)
	{
		int result =itemService.saveItemCompanyType(itemCompany);
		return "AddItemCompany";
	}

	@RequestMapping("/AddItem")
	public String AddItem(Model model, HttpServletRequest req,HttpServletResponse res)
	{
		List<ItemType> itemTypeList=itemService.getItemTypeList();
		List<ItemCompany> itemCompanyList=itemService.getItemCompanyList();
		
		model.addAttribute("itemCompanyList",itemCompanyList);
		model.addAttribute("itemTypeList",itemTypeList);
		return "AddItem";
	}

	@RequestMapping(value="/AddItem", method=RequestMethod.POST)
	public String AddItem(@ModelAttribute Item item, Model model, HttpServletRequest req,HttpServletResponse res)
	{
		int result =itemService.saveItem(item);
		return "AddItem";
	}

}
