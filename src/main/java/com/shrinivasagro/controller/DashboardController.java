package com.shrinivasagro.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.shrinivasagro.model.Login;

@Controller
public class DashboardController {

	@RequestMapping("/Dashboard")
	public String Dashboard(Model model, HttpServletRequest req,HttpServletResponse res)
	{

		return "Dashboard";

	}
}
