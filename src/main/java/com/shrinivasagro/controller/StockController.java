package com.shrinivasagro.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.shrinivasagro.dto.StockDto;
import com.shrinivasagro.model.Customer;
import com.shrinivasagro.model.CustomerItemHistory;
import com.shrinivasagro.model.ItemSize;
import com.shrinivasagro.model.ItemType;
import com.shrinivasagro.model.ItemUnit;
import com.shrinivasagro.model.Stock;
import com.shrinivasagro.model.StockHistory;
import com.shrinivasagro.services.ItemService;
import com.shrinivasagro.services.StockService;

@Controller
public class StockController {


	@Autowired
	StockService stockService;
	@Autowired
	ItemService itemService;

	@RequestMapping("/StockMaster")
	public String StockMaster(Model model, HttpServletRequest req,HttpServletResponse res)
	{
		List<StockDto> stockList=stockService.getStockAllList();
		model.addAttribute("stockList",stockList);
		return "StockMaster";
	}


	@RequestMapping("/AddStock")
	public String AddCustomer(Model model, HttpServletRequest req,HttpServletResponse res)
	{

		List<ItemType> itemTypeList=itemService.getItemTypeList();
		List<ItemSize> itemSizeList= itemService.getItemSizeList();
		List<ItemUnit> itemUnitList= itemService.getItemUnitList();

		model.addAttribute("itemUnitList",itemUnitList);
		model.addAttribute("itemSizeList",itemSizeList);
		model.addAttribute("itemTypeList",itemTypeList);
		return "AddStock";
	}


	@RequestMapping(value="/AddStock", method=RequestMethod.POST)
	public String AddStock(@ModelAttribute Stock stock, Model model, HttpServletRequest req,HttpServletResponse res)
	{

		Stock stock1=stockService.addStockItem(stock);


		List<ItemType> itemTypeList=itemService.getItemTypeList();
		List<ItemSize> itemSizeList= itemService.getItemSizeList();
		List<ItemUnit> itemUnitList= itemService.getItemUnitList();

		model.addAttribute("itemUnitList",itemUnitList);
		model.addAttribute("itemSizeList",itemSizeList);
		model.addAttribute("itemTypeList",itemTypeList);
		return "AddStock";
	}

	@RequestMapping("/StockHistory")
	public String StockHistory(Model model, HttpServletRequest req,HttpServletResponse res)
	{
		List<StockDto> stockList=stockService.getStockHistoryAllList();
		model.addAttribute("stockList",stockList);
		return "StockHistory";
	}

}
