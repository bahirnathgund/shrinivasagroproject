package com.shrinivasagro.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.shrinivasagro.model.ItemPrice;

public interface ItemPriceRepository extends CrudRepository<ItemPrice, String>{

	List findAll();
	
	ItemPrice findByItemTypeIdAndItemIdAndItemCompanyIdAndSizeIdAndItemUnitId(int itemTypeId, int itemId, int itemCompanyId, int sizeId, int itemUnitId);
}
