package com.shrinivasagro.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.shrinivasagro.model.ItemCompany;

public interface ItemCompanyRepository extends CrudRepository<ItemCompany, String>{

	List findAll();
	
	List findByItemTypeId(int itemTypeId);
}
