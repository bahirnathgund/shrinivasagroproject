package com.shrinivasagro.repository;

import org.springframework.data.repository.CrudRepository;

import com.shrinivasagro.model.Login;

public interface LoginRepository extends CrudRepository<Login, String>{

	Login findByUserNameAndPassword(String username, String password);
}
