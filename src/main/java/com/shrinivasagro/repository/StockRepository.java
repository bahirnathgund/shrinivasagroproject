package com.shrinivasagro.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.shrinivasagro.model.Stock;

public interface StockRepository extends CrudRepository<Stock, String>{

	Stock findByItemIdAndItemTypeIdAndItemCompanyIdAndSizeId(int itemId, int itemTypeId, int itemCompanyId, int sizeId);

	List findAll();
}
