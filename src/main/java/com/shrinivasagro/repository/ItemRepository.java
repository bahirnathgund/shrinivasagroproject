package com.shrinivasagro.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.shrinivasagro.model.Item;
import com.shrinivasagro.model.ItemType;


public interface ItemRepository extends CrudRepository<Item, String>{

	List findAll();

	void save(ItemType itemType);
	
	List findByItemTypeId(int itemTypeId);
}
