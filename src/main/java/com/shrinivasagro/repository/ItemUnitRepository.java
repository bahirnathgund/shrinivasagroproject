package com.shrinivasagro.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.shrinivasagro.model.ItemUnit;

public interface ItemUnitRepository extends CrudRepository<ItemUnit, String>{

	List findAll();
}
