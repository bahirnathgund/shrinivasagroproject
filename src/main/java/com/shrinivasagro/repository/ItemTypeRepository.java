package com.shrinivasagro.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.shrinivasagro.model.ItemType;
public interface ItemTypeRepository extends CrudRepository<ItemType, String>{

	List findAll();
}
