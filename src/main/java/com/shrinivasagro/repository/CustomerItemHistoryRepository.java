package com.shrinivasagro.repository;

import org.springframework.data.repository.CrudRepository;

import com.shrinivasagro.model.CustomerItemHistory;

public interface CustomerItemHistoryRepository extends CrudRepository<CustomerItemHistory, String>{

}
