package com.shrinivasagro.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.shrinivasagro.model.Customer;

public interface CustomerRepository extends CrudRepository<Customer, String>{

	List findAll();
	
}
