package com.shrinivasagro.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.shrinivasagro.model.ItemSize;

public interface ItemSizeRepository extends CrudRepository<ItemSize, String>{

	List findAll();
}
