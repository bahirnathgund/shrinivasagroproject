package com.shrinivasagro.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.shrinivasagro.model.StockHistory;

public interface StockHistoryRepository extends CrudRepository<StockHistory, String>{

	List findAll();
}
