package com.shrinivasagro;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import java.security.SecureRandom;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
@ComponentScan(basePackages={"com.shrinivasagro"})
public class ShrinivasAgroApplication {

	public static void main(String[] args) {
		SpringApplication.run(ShrinivasAgroApplication.class, args);
	}
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(ShrinivasAgroApplication.class);
	}	
	@Bean
	public SecureRandom secureRandom()
	{
		return new SecureRandom();
	}
}
