package com.shrinivasagro.services;

import java.util.List;

import com.shrinivasagro.model.Customer;
import com.shrinivasagro.model.CustomerItemHistory;

public interface CustomerService {

	List<Customer> getCustomerList();

	Customer saveCustomer(Customer customer);

	void addCustomerItem(int itemTypeId, int itemCompanyId, int itemId, int sizeId, int itemUnitId,
			double itemInitialPrice, double itemQty, double gstAmount, double totalAmount);

	void saveCustomerItemList(CustomerItemHistory customerItemHistory);

}
