package com.shrinivasagro.services;

import java.util.List;

import com.shrinivasagro.model.ItemPrice;

public interface ItemPriceService {

	List<ItemPrice> getItemPriceList();

	int saveiItemPrice(ItemPrice itemPrice);

	ItemPrice getItemPriceDetails(int itemTypeId, int itemCompanyId, int itemId, int sizeId, int itemUnitId);

}
