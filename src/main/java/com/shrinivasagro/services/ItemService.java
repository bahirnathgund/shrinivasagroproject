package com.shrinivasagro.services;

import java.util.List;

import com.shrinivasagro.model.Item;
import com.shrinivasagro.model.ItemCompany;
import com.shrinivasagro.model.ItemSize;
import com.shrinivasagro.model.ItemType;
import com.shrinivasagro.model.ItemUnit;

public interface ItemService {

	List<ItemType> getItemTypeList();

	List<ItemCompany> getItemCompanyList();

	List<Item> getItemList();

	int saveItemType(ItemType itemType);

	int saveItemCompanyType(ItemCompany itemCompany);

	int saveItem(Item item);

	List<Item> getItemListByItemType(int itemTypeId);

	List<ItemCompany> getItemCompamyListByItemType(int itemTypeId);

	List<ItemSize> getItemSizeList();

	List<ItemUnit> getItemUnitList();

}
