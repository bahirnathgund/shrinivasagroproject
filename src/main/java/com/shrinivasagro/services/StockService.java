package com.shrinivasagro.services;

import java.util.List;

import com.shrinivasagro.dto.StockDto;
import com.shrinivasagro.model.CustomerItemHistory;
import com.shrinivasagro.model.Stock;

public interface StockService {

	void deductItemFromStock(CustomerItemHistory customerItemHistory);

	void addStockHistory(CustomerItemHistory customerItemHistory);

	List<StockDto> getStockAllList();

	Stock addStockItem(Stock stock);

	List<StockDto> getStockHistoryAllList();

}
