package com.shrinivasagro.services;

import com.shrinivasagro.model.Login;

public interface LoginService {


	Login getUserDetails(String username, String password);

}
