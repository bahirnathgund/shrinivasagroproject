package com.shrinivasagro.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.shrinivasagro.model.Item;
import com.shrinivasagro.model.ItemCompany;
import com.shrinivasagro.model.ItemSize;
import com.shrinivasagro.model.ItemType;
import com.shrinivasagro.model.ItemUnit;
import com.shrinivasagro.repository.ItemCompanyRepository;
import com.shrinivasagro.repository.ItemRepository;
import com.shrinivasagro.repository.ItemSizeRepository;
import com.shrinivasagro.repository.ItemTypeRepository;
import com.shrinivasagro.repository.ItemUnitRepository;
import com.shrinivasagro.services.ItemService;

@Component
public class ItemServiceImpl implements ItemService{

	@Autowired
	ItemTypeRepository itemTypeRepository;
	@Autowired
	ItemCompanyRepository itemCompanyRepository;
	@Autowired
	ItemRepository itemRepository;
	@Autowired
	ItemSizeRepository itemSizeRepository;
	@Autowired
	ItemUnitRepository itemUnitRepository;

	
	@Override
	public List<ItemType> getItemTypeList() {
		// TODO Auto-generated method stub
		List<ItemType> itemTypeList=itemTypeRepository.findAll();
		return itemTypeList;
	}

	@Override
	public List<ItemCompany> getItemCompanyList() {
		// TODO Auto-generated method stub
		List<ItemCompany> itemCompanyList=itemCompanyRepository.findAll();
		return itemCompanyList;
	}

	@Override
	public List<Item> getItemList() {
		// TODO Auto-generated method stub
		List<Item> itemList=itemRepository.findAll();
		List<ItemType> itemTypeList=itemTypeRepository.findAll();
		for(Item item : itemList)
		{
			for(ItemType itemType : itemTypeList)
			{
				if(item.getItemTypeId()==itemType.getId())
				{
					item.setItemType(itemType.getTypeName());
					break;
				}
			}
		}
		return itemList;
	}

	@Override
	public int saveItemType(ItemType itemType) {
		// TODO Auto-generated method stub
		itemTypeRepository.save(itemType);
		return 1;
	}

	@Override
	public int saveItemCompanyType(ItemCompany itemCompany) {
		// TODO Auto-generated method stub
		itemCompanyRepository.save(itemCompany);
		return 1;
	}

	@Override
	public int saveItem(Item item) {
		// TODO Auto-generated method stub
		item.setStatus(1);
		itemRepository.save(item);
		return 1;
	}

	@Override
	public List<Item> getItemListByItemType(int itemTypeId) {
		// TODO Auto-generated method stub
		List<Item> itemList = itemRepository.findByItemTypeId(itemTypeId);
		return itemList;
	}

	@Override
	public List<ItemCompany> getItemCompamyListByItemType(int itemTypeId) {
		// TODO Auto-generated method stub
		try {
		List<ItemCompany> itemCompanyList = itemCompanyRepository.findByItemTypeId(itemTypeId);
		return itemCompanyList;
		
		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public List<ItemSize> getItemSizeList() {
		// TODO Auto-generated method stub
		List<ItemSize> itemSizeList =itemSizeRepository.findAll();
		return itemSizeList;
	}

	@Override
	public List<ItemUnit> getItemUnitList() {
		// TODO Auto-generated method stub
		List<ItemUnit> itemUnitList = itemUnitRepository.findAll();
		return itemUnitList;
	}

}
