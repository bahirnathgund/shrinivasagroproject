package com.shrinivasagro.serviceImpl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.shrinivasagro.model.ItemPrice;
import com.shrinivasagro.repository.ItemPriceRepository;
import com.shrinivasagro.services.ItemPriceService;

@Component
public class ItemPriceServiceImpl implements ItemPriceService{

	@Autowired
	ItemPriceRepository itemPriceRepository;

	@Override
	public List<ItemPrice> getItemPriceList() {
		// TODO Auto-generated method stub
		List<ItemPrice> itemPriceList= itemPriceRepository.findAll();
		return itemPriceList;
	}

	@Override
	public int saveiItemPrice(ItemPrice itemPrice) {
		// TODO Auto-generated method stub
		try {
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");  
		Date date = new Date(); 
		itemPrice.setCreatedDate(formatter.format(date));
		itemPrice.setUpdateDate(formatter.format(date));
		itemPrice.setItemStatus(1);
		System.out.println("getItemInitialPrice = "+itemPrice.getItemInitialPrice());
		itemPriceRepository.save(itemPrice);
		return 1;
		}catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
			return 0;
		}
	}

	@Override
	public ItemPrice getItemPriceDetails(int itemTypeId, int itemCompanyId, int itemId, int sizeId, int itemUnitId) {
		// TODO Auto-generated method stub
		ItemPrice itemPrice= itemPriceRepository.findByItemTypeIdAndItemIdAndItemCompanyIdAndSizeIdAndItemUnitId(itemTypeId,itemId, itemCompanyId, sizeId, itemUnitId);
		return itemPrice;
	}

}
