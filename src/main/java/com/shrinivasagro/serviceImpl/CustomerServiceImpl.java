package com.shrinivasagro.serviceImpl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.shrinivasagro.model.Customer;
import com.shrinivasagro.model.CustomerItemHistory;
import com.shrinivasagro.repository.CustomerItemHistoryRepository;
import com.shrinivasagro.repository.CustomerRepository;
import com.shrinivasagro.services.CustomerService;

@Component
public class CustomerServiceImpl implements CustomerService{

	@Autowired
	CustomerRepository customerRepository;
	@Autowired
	CustomerItemHistoryRepository customerItemHistoryRepository;

	@Override
	public List<Customer> getCustomerList() {
		// TODO Auto-generated method stub
		List<Customer> customerlist = customerRepository.findAll();
		return customerlist;
	}

	@Override
	public Customer saveCustomer(Customer customer) {
		// TODO Auto-generated method stub
		try {
			customer.setCreatedDate(new Date());
			Customer customer1=customerRepository.save(customer);

			return customer1;
		}catch (Exception e) {
			// TODO: handle exception
			return null;
		}
	}

	@Override
	public void addCustomerItem(int itemTypeId, int itemCompanyId, int itemId, int sizeId, int itemUnitId,
			double itemInitialPrice, double itemQty, double gstAmount, double totalAmount) {
		CustomerItemHistory customerItemHistory=new CustomerItemHistory();

		customerItemHistory.setItemId(itemId);
		customerItemHistory.setItemTypeId(itemTypeId);
		customerItemHistory.setItemCompanyId(itemCompanyId);
		customerItemHistory.setSizeId(sizeId);
		customerItemHistory.setItemUnitId(itemUnitId);
		customerItemHistory.setItemPrice(itemInitialPrice);
		customerItemHistory.setItemQty(itemQty);
		customerItemHistory.setItemdiscount(0);
		customerItemHistory.setGstAmount(gstAmount);
		customerItemHistory.setTotalAmount(totalAmount);
		customerItemHistoryRepository.save(customerItemHistory);
	}

	@Override
	public void saveCustomerItemList(CustomerItemHistory customerItemHistory) {
		// TODO Auto-generated method stub
		try {
			
			CustomerItemHistory customerItemHistory1=customerItemHistoryRepository.save(customerItemHistory);
		}catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}
	}


}
