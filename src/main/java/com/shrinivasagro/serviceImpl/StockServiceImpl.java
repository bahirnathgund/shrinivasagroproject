package com.shrinivasagro.serviceImpl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.shrinivasagro.dto.StockDto;
import com.shrinivasagro.model.CustomerItemHistory;
import com.shrinivasagro.model.Item;
import com.shrinivasagro.model.ItemCompany;
import com.shrinivasagro.model.ItemSize;
import com.shrinivasagro.model.ItemType;
import com.shrinivasagro.model.Stock;
import com.shrinivasagro.model.StockHistory;
import com.shrinivasagro.repository.CustomerRepository;
import com.shrinivasagro.repository.ItemCompanyRepository;
import com.shrinivasagro.repository.StockHistoryRepository;
import com.shrinivasagro.repository.StockRepository;
import com.shrinivasagro.services.ItemService;
import com.shrinivasagro.services.StockService;

@Component
public class StockServiceImpl implements StockService{

	@Autowired
	StockRepository stockRepository;
	@Autowired
	StockHistoryRepository stockHistoryRepository;
	@Autowired
	ItemService itemService;
	@Autowired
	ItemCompanyRepository itemCompanyRepository;

	@Override
	public void deductItemFromStock(CustomerItemHistory customerItemHistory) {
		// TODO Auto-generated method stub
		try {
			Stock stock1=stockRepository.findByItemIdAndItemTypeIdAndItemCompanyIdAndSizeId(customerItemHistory.getItemId(), customerItemHistory.getItemTypeId(), customerItemHistory.getItemCompanyId(), customerItemHistory.getSizeId());
			double currentStock=0;
			if(stock1!=null)
			{
				currentStock=stock1.getCurrentStock()-customerItemHistory.getItemQty();
				stock1.setId(stock1.getId());
				stock1.setCurrentStock(currentStock);
				stockRepository.save(stock1);
			}
		}catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public void addStockHistory(CustomerItemHistory customerItemHistory) {
		// TODO Auto-generated method stub
		try {
			StockHistory stockHistory=new StockHistory();
			stockHistory.setItemId(customerItemHistory.getItemId());
			stockHistory.setItemTypeId(customerItemHistory.getItemTypeId());
			stockHistory.setItemCompanyId(customerItemHistory.getItemCompanyId());
			stockHistory.setSizeId(customerItemHistory.getSizeId());
			stockHistory.setStockValue(customerItemHistory.getItemQty());
			stockHistory.setCredit_DebitStatus(2);
			stockHistory.setCreateBy(1);
			stockHistory.setCreateDate(new Date());
			stockHistoryRepository.save(stockHistory);

		}catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}

	}

	@Override
	public List<StockDto> getStockAllList() {
		// TODO Auto-generated method stub

		List<StockDto> StockDtoList =new ArrayList<StockDto>();
		try {
			List<Stock> stock=stockRepository.findAll();

			List<Item> itemList=itemService.getItemList();
			List<ItemType> itemTypeList=itemService.getItemTypeList();
			List<ItemCompany> itemCompanyList=itemCompanyRepository.findAll();
			List<ItemSize> itemSizeList= itemService.getItemSizeList();
			StockDto stockDto=new StockDto();
			for(int i=0;i<stock.size();i++)
			{
				stockDto=new StockDto();
				stockDto.setStockId(stock.get(i).getId());
				stockDto.setCurrentStock(stock.get(i).getCurrentStock());
				stockDto.setItemId(stock.get(i).getItemId());
				stockDto.setItemTypeId(stock.get(i).getItemTypeId());
				stockDto.setItemCompanyId(stock.get(i).getItemCompanyId());
				stockDto.setSizeId(stock.get(i).getSizeId());
				for(int j=0;j<itemList.size();j++)
				{
					if(stock.get(i).getItemId()==itemList.get(j).getId())
					{
						stockDto.setItemName(itemList.get(j).getItemName());
						break;
					}
				}
				for(int j=0;j<itemTypeList.size();j++)
				{
					if(stock.get(i).getItemTypeId()==itemTypeList.get(j).getId())
					{
						stockDto.setTypeName(itemTypeList.get(j).getTypeName());
						break;
					}
				}

				for(int j=0;j<itemSizeList.size();j++)
				{
					if(stock.get(i).getSizeId()==itemSizeList.get(j).getId())
					{
						stockDto.setItemSize(itemSizeList.get(j).getItemSize());
						break;
					}
				}

				for(int j=0;j<itemCompanyList.size();j++)
				{
					if(stock.get(i).getItemCompanyId()==itemCompanyList.get(j).getId())
					{
						stockDto.setItemCompanyName(itemCompanyList.get(j).getItemCompanyName());
						break;
					}
				}
				StockDtoList.add(stockDto);

			}
		}catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}
		return StockDtoList;
	}

	@Override
	public Stock addStockItem(Stock stock) {
		Stock stock1=null;
		try {
			stock1=stockRepository.findByItemIdAndItemTypeIdAndItemCompanyIdAndSizeId(stock.getItemId(), stock.getItemTypeId(), stock.getItemCompanyId(), stock.getSizeId());
			double currentStock=0;
			if(stock1!=null)
			{
				currentStock=stock1.getCurrentStock()+stock.getCurrentStock();
				stock1.setId(stock1.getId());
				stock1.setCurrentStock(currentStock);
				stockRepository.save(stock1);
			}
			else
			{
				stock1=new Stock();
				stock1.setItemId(stock.getItemId());
				stock1.setItemTypeId(stock.getItemTypeId());
				stock1.setItemCompanyId(stock.getItemCompanyId());
				stock1.setSizeId(stock.getSizeId());
				stock1.setCurrentStock(stock.getCurrentStock());

				stockRepository.save(stock1);

			}
		}catch (Exception e) {
			// TODO: handle exception
		}
		try {
			StockHistory stockHistory=new StockHistory();
			stockHistory.setItemId(stock.getItemId());
			stockHistory.setItemTypeId(stock.getItemTypeId());
			stockHistory.setItemCompanyId(stock.getItemCompanyId());
			stockHistory.setSizeId(stock.getSizeId());
			stockHistory.setStockValue(stock.getCurrentStock());
			stockHistory.setCredit_DebitStatus(1);
			stockHistory.setCreateBy(1);
			stockHistory.setCreateDate(new Date());
			stockHistoryRepository.save(stockHistory);

		}catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}
		return stock1;
	}

	@Override
	public List<StockDto> getStockHistoryAllList() {

		List<StockDto> StockDtoList =new ArrayList<StockDto>();
		try {
			List<StockHistory> stock=stockHistoryRepository.findAll();

			List<Item> itemList=itemService.getItemList();
			List<ItemType> itemTypeList=itemService.getItemTypeList();
			List<ItemCompany> itemCompanyList=itemCompanyRepository.findAll();
			List<ItemSize> itemSizeList= itemService.getItemSizeList();
			StockDto stockDto=new StockDto();
			for(int i=0;i<stock.size();i++)
			{
				stockDto=new StockDto();
				stockDto.setStockId(stock.get(i).getId());
				stockDto.setCurrentStock(stock.get(i).getStockValue());
				stockDto.setItemId(stock.get(i).getItemId());
				stockDto.setItemTypeId(stock.get(i).getItemTypeId());
				stockDto.setItemCompanyId(stock.get(i).getItemCompanyId());
				stockDto.setSizeId(stock.get(i).getSizeId());
				for(int j=0;j<itemList.size();j++)
				{
					if(stock.get(i).getItemId()==itemList.get(j).getId())
					{
						stockDto.setItemName(itemList.get(j).getItemName());
						break;
					}
				}
				for(int j=0;j<itemTypeList.size();j++)
				{
					if(stock.get(i).getItemTypeId()==itemTypeList.get(j).getId())
					{
						stockDto.setTypeName(itemTypeList.get(j).getTypeName());
						break;
					}
				}

				for(int j=0;j<itemSizeList.size();j++)
				{
					if(stock.get(i).getSizeId()==itemSizeList.get(j).getId())
					{
						stockDto.setItemSize(itemSizeList.get(j).getItemSize());
						break;
					}
				}

				for(int j=0;j<itemCompanyList.size();j++)
				{
					if(stock.get(i).getItemCompanyId()==itemCompanyList.get(j).getId())
					{
						stockDto.setItemCompanyName(itemCompanyList.get(j).getItemCompanyName());
						break;
					}
				}
				if(stock.get(i).getCredit_DebitStatus()==1)
				{
					stockDto.setInout("IN");
				}
				else if(stock.get(i).getCredit_DebitStatus()==2)
				{
					stockDto.setInout("OUT");
				}
				StockDtoList.add(stockDto);

			}
		}catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}
		return StockDtoList;
	}

}
