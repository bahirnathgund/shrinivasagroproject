package com.shrinivasagro.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.shrinivasagro.model.Login;
import com.shrinivasagro.repository.LoginRepository;
import com.shrinivasagro.services.LoginService;

@Component
public class LoginServiceImpl implements LoginService{

	@Autowired
	LoginRepository loginRepository;
	@Override
	public Login getUserDetails(String username, String password) {
		// TODO Auto-generated method stub
		Login login =loginRepository.findByUserNameAndPassword(username, password);
		return login;
	}

}
