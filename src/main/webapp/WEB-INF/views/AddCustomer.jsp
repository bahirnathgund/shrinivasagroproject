<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description"
	content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
<meta name="author" content="Coderthemes">

<!-- App Favicon -->
<link rel="shortcut icon"
	href="${pageContext.request.contextPath}/resources/images/favicon.ico">

<!-- App title -->
<title>Add Customer</title>

<!-- Switchery css -->
<link
	href="${pageContext.request.contextPath}/resources/plugins/switchery/switchery.min.css"
	rel="stylesheet" />

<!-- Bootstrap CSS -->
<link
	href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css"
	rel="stylesheet" type="text/css" />

<!-- App CSS -->
<link href="${pageContext.request.contextPath}/resources/css/style.css"
	rel="stylesheet" type="text/css" />

<!-- Modernizr js -->
<script
	src="${pageContext.request.contextPath}/resources/js/modernizr.min.js"></script>


</head>


<body class="fixed-left" onLoad="init()">


	<div id="wrapper">

		<%@ include file="headerpage.jsp"%>

		<%@ include file="menu.jsp"%>

		<div class="content-page">

			<div class="content">
				<div class="container-fluid">

					<div class="row">
						<div class="col-xl-12">
							<div class="page-title-box">
								<h4 class="page-title float-left">Add Customer</h4>

								<ol class="breadcrumb float-right">
									<li class="breadcrumb-item"><a href="home">Home</a></li>
									<li class="breadcrumb-item"><a href="CustomerMaster">
											Customer Master</a></li>
									<li class="breadcrumb-item active">Add Customer</li>
								</ol>

								<div class="clearfix"></div>
							</div>
						</div>
					</div>

					<form name="customerform"
						action="${pageContext.request.contextPath}/AddCustomer"
						onSubmit="return validate()" method="post">

						<div class="row">

							<div class="col-12">

								<div class="card-box">


									<div class="row">

										<div class="col-xl-3">
											<div class="form-group">
												<label for="customerName">Customer Name<span
													class="text-danger">*</span></label> <input class="form-control"
													type="text" id="customerName" name="customerName"
													placeholder="customer Name" style="text-transform: capitalize;">
											</div>
											<span id="customerNameSpan" style="color: #FF0000"></span>
										</div>
										
										<div class="col-xl-3">
											<div class="form-group">
												<label for="address">Address<span
													class="text-danger">*</span></label> <input class="form-control"
													type="text" id="address" name="address"
													placeholder="address" style="text-transform: capitalize;">
											</div>
											<span id="addressSpan" style="color: #FF0000"></span>
										</div>
										
										<div class="col-xl-3">
											<div class="form-group">
												<label for="pincode">Pin code<span
													class="text-danger">*</span></label> <input class="form-control"
													type="text" id="pincode" name="pincode"
													placeholder="pincode" style="text-transform: capitalize;">
											</div>
											<span id="pincodeSpan" style="color: #FF0000"></span>
										</div>
										
										
										<div class="col-xl-3">
											<div class="form-group">
												<label for="mobileNo">Mobile No<span
													class="text-danger">*</span></label> <input class="form-control"
													type="text" id="mobileNo" name="mobileNo"
													placeholder="mobileNo" style="text-transform: capitalize;">
											</div>
											<span id="mobileNoSpan" style="color: #FF0000"></span>
										</div>
										
										<div class="col-xl-3">
											<div class="form-group">
												<label for="emailId">EmailId</label> <input class="form-control"
													type="text" id="emailId" name="emailId"
													placeholder="emailId" style="text-transform: capitalize;">
											</div>
											<span id="emailIdSpan" style="color: #FF0000"></span>
										</div>
										
									</div>
									
									
									<div class="row">	
										
										<div class="col-xl-3">
											<div class="form-group">
												<label for="itemTypeId">Item Type<span
													class="text-danger">*</span></label> <select class="form-control"
													name="itemTypeId" id="itemTypeId"  onchange="getItemCompamyAndItemList(this.value)">
													<option selected="selected" value="Default">-Select
														type-</option>

													<c:forEach var="itemTypeList" items="${itemTypeList}">
														<option value="${itemTypeList.id}">${itemTypeList.typeName}</option>
													</c:forEach>
												</select>
											</div>
											<span id="itemTypeIdSpan" style="color: #FF0000"></span>
										</div>

										<div class="col-xl-3">
											<div class="form-group">
												<label for="itemId">Item Name<span
													class="itemId">*</span></label> <select class="form-control"
													name="itemId" id="itemId"
													onchange="getItemPriceDetails(this.value)">
													<option selected="selected" value="Default">-Select
														Item-</option>
												</select>
											</div>
											<span id="itemIdSpan" style="color: #FF0000"></span>
										</div>


										<div class="col-xl-3">
											<div class="form-group">
												<label for="itemCompanyId">Company Name<span
													class="text-danger">*</span></label> <select class="form-control"
													name="itemCompanyId" id="itemCompanyId"
													onchange="getItemPriceDetails(this.value)">
													<option selected="selected" value="Default">-Select
														Company-</option>
												</select>
											</div>
											<span id="itemCompanyIdSpan" style="color: #FF0000"></span>
										</div>

										<div class="col-xl-2">
											<div class="form-group">
												<label for="sizeId">Item Size<span
													class="text-danger">*</span></label> <select class="form-control"
													name="sizeId" id="sizeId" onchange="getItemPriceDetails(this.value)">
													<option selected="selected" value="Default">-Select
														Size-</option>

													<c:forEach var="itemSizeList" items="${itemSizeList}">
														<option value="${itemSizeList.id}">${itemSizeList.itemSize}</option>
													</c:forEach>
												</select>
											</div>
											<span id="sizeIdSpan" style="color: #FF0000"></span>
										</div>

										<div class="col-xl-3">
											<div class="form-group">
												<label for="itemUnitId">Item Unit<span
													class="text-danger">*</span></label> <select class="form-control"
													name="itemUnitId" id="itemUnitId" onchange="getItemPriceDetails(this.value)">
													<option selected="selected" value="Default">-Select
														Unit-</option>

													<c:forEach var="itemUnitList" items="${itemUnitList}">
														<option value="${itemUnitList.id}">${itemUnitList.itemUnit}</option>
													</c:forEach>
												</select>
											</div>
											<span id="itemUnitIdSpan" style="color: #FF0000"></span>
										</div>


										<div class="col-xl-2">
											<div class="form-group">
												<label for="itemInitialPrice">Item Initial Price<span
													class="text-danger">*</span></label> <input class="form-control"
													type="text" id="itemInitialPrice" name="itemInitialPrice"
													placeholder="itemInitialPrice" style="text-transform: capitalize;">
											</div>
											<span id="itemInitialPriceSpan" style="color: #FF0000"></span>
										</div>

										<div class="col-xl-2">
											<div class="form-group">
												<label for="itemQty">Item Qty<span
													class="text-danger">*</span></label> <input class="form-control"
													type="text" id="itemQty" name="itemQty"
													placeholder="itemQty" style="text-transform: capitalize;">
											</div>
											<span id="itemQtySpan" style="color: #FF0000"></span>
										</div>


										<div class="col-xl-3">
											<div class="form-group">
												<label for="itemQty">Total</label>
												<div class="input-group">
													<input class="form-control" type="text"
														id="itemQty" name="itemQty"
														readonly="readonly"> <span class="input-group-btn">
														<button type="button" class="btn btn-success"
															onclick="return AddCustomerItemList()">
															<i class="fa fa-plus"></i>Add
														</button>
													</span>
												</div>
											</div>

											<span id="itemQtySpan" style="color: #FF0000"></span>
										</div>

									</div>


									<div class="row">

										<div class="col-xl-12">
											<div class="card-box table-responsive">
												<h4 class="m-t-0 header-title">Item Table</h4>
												<span id="totalNumberOfGoldItemsSpan" style="color: #FF0000"></span>

												<table id="customerItemTable"
													class="table table-striped table-bordered table mb-0 table-sm">
													<thead>
														<tr>
															<th>#</th>
															<th>Item</th>
															<th>Type</th>
															<th>Company Name</th>
															<th>Size</th>
															<th>Unit</th>
															<th>Price</th>
															<th>Qty</th>
															<th>Total</th>
															
															
															<th hidden="hidden">*</th>
															<th hidden="hidden">*</th>
															<th hidden="hidden">*</th>
															<th hidden="hidden">*</th>
															<th hidden="hidden">*</th>
															<th style="width: 50px">Action</th>
														</tr>
													</thead>
													<tbody>

													</tbody>
												</table>

											</div>
										</div>
									</div>


									<br />
									<div class="row">
										<div class="col-xl-2"></div>
										<div class="col-xl-3">
											<a href="CustomerMaster"><button type="button"
													class="btn btn-secondary" value="reset" style="width: 90px">Back</button></a>
										</div>

										<div class="col-xl-3">
											<button type="reset" class="btn btn-default">Reset</button>
										</div>

										<div class="col-xl-3">
											<button class="btn btn-primary" type="submit">Submit</button>
										</div>
									</div>


								
								     	<input type="hidden" id="itemIds" name="itemIds" value="">
								     	<input type="hidden" id="itemTypeIds" name="itemTypeIds" value="">
								     	<input type="hidden" id="itemCompanyIds" name="itemCompanyIds" value="">
								     	<input type="hidden" id="sizeIds" name="sizeIds" value="">
								     	<input type="hidden" id="itemUnitIds" name="itemUnitIds" value="">
								     	<input type="hidden" id="itemQtys" name="itemQtys" value="">
								     	<input type="hidden" id="itemPrices" name="itemPrices" value="">
								     	<input type="hidden" id="itemdiscounts" name="itemdiscounts" value="">
								     	<input type="hidden" id="gstAmounts" name="gstAmounts" value="">
								     	<input type="hidden" id="totalAmounts" name="totalAmounts" value="">
								     

								</div>

							</div>
							<!-- end col-->

						</div>

					</form>

				</div>
				<!-- container -->
			</div>

			<%@ include file="RightSidebar.jsp"%>

			<%@ include file="footer.jsp"%>

		</div>
	</div>
	<script>
		var resizefunc = [];
	</script>

	<!-- jQuery  -->
	<script
		src="${pageContext.request.contextPath}/resources/js/jquery.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/js/popper.min.js"></script>
	<!-- Tether for Bootstrap -->
	<script
		src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
	<script src="${pageContext.request.contextPath}/resources/js/detect.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/js/fastclick.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/js/jquery.blockUI.js"></script>
	<script src="${pageContext.request.contextPath}/resources/js/waves.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/js/jquery.nicescroll.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/js/jquery.scrollTo.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/js/jquery.slimscroll.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/plugins/switchery/switchery.min.js"></script>

	<script
		src="${pageContext.request.contextPath}/resources/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js"
		type="text/javascript"></script>

	<script
		src="${pageContext.request.contextPath}/resources/js/jquery.core.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/js/jquery.app.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/plugins/autoNumeric/autoNumeric.js"
		type="text/javascript"></script>

	<script>
		function clearAll() {

			$('#itemCompanyNameSpan').html('');
			$('#itemTypeIdSpan').html('');

		}

		function init() {
			clearAll();
			document.customerform.itemCompanyName.value = "";
			document.customerform.itemCompanyName.focus();
		}

		function validate() {

			clearAll();
			if (document.customerform.itemCompanyName.value == "") {
				$('#itemCompanyNameSpan').html('Please, enter  Name..!');
				document.customerform.itemCompanyName.focus();
				return false;
			} else if (document.customerform.itemCompanyName.value.match(/^[\s]+$/)) {
				$('#itemCompanyNameSpan').html('Please, enter Name..!');
				document.customerform.itemCompanyName.value = "";
				document.customerform.itemCompanyName.focus();
				return false;
			}

			if (document.customerform.itemTypeId.value == "Default") {
				$('#itemTypeIdSpan').html('Please, select Type..!');
				document.customerform.itemTypeId.focus();
				return false;
			}

		}
		

		function getItemCompamyAndItemList()
		{
			 $("#itemCompanyId").empty();
			 var itemTypeId = $('#itemTypeId').val();
			
			$.ajax({

				url : '${pageContext.request.contextPath}/getItemCompamyListByItemType',
				type : 'Post',
				data : { itemTypeId : itemTypeId},
				dataType : 'json',
				success : function(result)
						  {
								if (result) 
								{
									var option = $('<option/>');
									option.attr('value',"Default").text("-Select Company-");
									$("#itemCompanyId").append(option);
									
									for(var i=0;i<result.length;i++)
									{
										var option = $('<option />');
									    option.attr('value',result[i].id).text(result[i].itemCompanyName);
									    $("#itemCompanyId").append(option);
									 } 
								} 
								else
								{
									alert("failure111");
									//$("#ajax_div").hide();
								}

							}
				});


			 $("#itemId").empty();
			$.ajax({

				url : '${pageContext.request.contextPath}/getItemListByItemType',
				type : 'Post',
				data : { itemTypeId : itemTypeId},
				dataType : 'json',
				success : function(result)
						  {
								if (result) 
								{
									var option = $('<option/>');
									option.attr('value',"Default").text("-Select Item-");
									$("#itemId").append(option);
									
									for(var i=0;i<result.length;i++)
									{
										var option = $('<option />');
									    option.attr('value',result[i].id).text(result[i].itemName);
									    $("#itemId").append(option);
									 } 
								} 
								else
								{
									alert("failure111");
									//$("#ajax_div").hide();
								}

							}
				});
			
		}

		function getItemPriceDetails()
		{
			 $("#itemInitialPrice").empty();
			 
			 var itemTypeId = $('#itemTypeId').val();
			 var itemCompanyId = $('#itemCompanyId').val();
			 var itemId = $('#itemId').val();
			 var sizeId = $('#sizeId').val();
			 var itemUnitId = $('#itemUnitId').val();
			
			$.ajax({

				url : '${pageContext.request.contextPath}/getItemPriceDetails',
				type : 'Post',
				data : { itemTypeId : itemTypeId, itemCompanyId : itemCompanyId, itemId : itemId, sizeId : sizeId, itemUnitId : itemUnitId},
				dataType : 'json',
				success : function(result)
						  {
								if (result) 
								{

									document.customerform.itemInitialPrice.value = result.itemInitialPrice;
								} 
								else
								{
									alert("failure111");
									//$("#ajax_div").hide();
								}

							}
				});


			
		}

		function AddCustomerItemList()
		{
			// $("#itemInitialPrice").empty();

			 
			 
			 
			 var itemId = $('#itemId').val();
			 var itemTypeId = $('#itemTypeId').val();
			 var itemCompanyId = $('#itemCompanyId').val();
			 var sizeId = $('#sizeId').val();
			 var itemUnitId = $('#itemUnitId').val();
			 var itemQty = Number($('#itemQty').val());
			 var itemPrice = Number($('#itemInitialPrice').val());
			 var itemdiscount = $('#itemdiscount').val();
			 var gstAmount = $('#gstAmount').val();
			 //var totalAmount = $('#totalAmount').val();
			 var totalAmount =itemQty*itemPrice;
			 
			 var e1 = document.getElementById("itemId");
			 var itemName = e1.options[e1.selectedIndex].text;
			 var e2 = document.getElementById("itemTypeId");
			 var itemTypeName = e2.options[e2.selectedIndex].text;
			 var e3 = document.getElementById("itemCompanyId");
			 var itemCompanyName = e3.options[e3.selectedIndex].text;
			 var e4 = document.getElementById("sizeId");
			 var itemSize = e4.options[e4.selectedIndex].text;
			 var e5 = document.getElementById("itemUnitId");
			 var itemUnit = e5.options[e5.selectedIndex].text;
			 
			  var rowlength1 = document.getElementById("customerItemTable").rows.length;
			  var table = document.getElementById("customerItemTable");
			  var row = table.insertRow(-1);
			  
			  
			  var cell0 = row.insertCell(0);
			  var cell1 = row.insertCell(1);
			  var cell2 = row.insertCell(2);
			  var cell3 = row.insertCell(3);
			  var cell4 = row.insertCell(4);
			  var cell5 = row.insertCell(5);
			  var cell6 = row.insertCell(6);
			  var cell7 = row.insertCell(7);
			  var cell8 = row.insertCell(8);
			  
			  cell0.innerHTML = Number(rowlength1);
			  cell1.innerHTML = itemName;
			  cell2.innerHTML = itemTypeName;
			  cell3.innerHTML = itemCompanyName;
			  cell4.innerHTML = itemSize;
			  cell5.innerHTML = itemUnit;
			  cell6.innerHTML = itemPrice;
			  cell7.innerHTML = itemQty;
			  cell8.innerHTML = totalAmount;
			  

			  var cell9 = row.insertCell(9);
			  var cell10 = row.insertCell(10);
			  var cell11 = row.insertCell(11);
			  var cell12 = row.insertCell(12);
			  var cell13 = row.insertCell(13);
			  
			  cell9.innerHTML = itemId;
			  cell10.innerHTML = itemTypeId;
			  cell11.innerHTML = itemCompanyId;
			  cell12.innerHTML = sizeId;
			  cell13.innerHTML = itemUnitId;
			  

			  var cell14 = row.insertCell(14);
			  cell14.innerHTML = '<a onclick="DeleteCustomerItem('+this+')" class="btn btn-danger fa fa-remove" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></a> ';
			 

	            var tbl = document.getElementById("customerItemTable");

                for (var i = 0; i < tbl.rows.length; i++) {
                            tbl.rows[i].cells[9].style.display = "none";
                            tbl.rows[i].cells[10].style.display = "none";
                            tbl.rows[i].cells[11].style.display = "none";
                            tbl.rows[i].cells[12].style.display = "none";
                            tbl.rows[i].cells[13].style.display = "none";
                }
			  

				 var itemIds ="";
				 var itemTypeIds ="";
				 var itemCompanyIds ="";
				 var sizeIds ="";
				 var itemUnitIds = "";
				 var itemQtys ="";
				 var itemPrices = "";
				 var itemdiscounts ="";
				 var gstAmounts = "";
				 var totalAmounts = "";
				 
				var oTable = document.getElementById('customerItemTable');
				var rowLength = oTable.rows.length;
				for (var i = 1; i < rowLength; i++){
					
					   var oCells = oTable.rows.item(i).cells;

					   var cellLength = oCells.length;
					   
					   itemId=oCells.item(9).innerHTML;
					   itemTypeId = oCells.item(10).innerHTML;
					   itemCompanyId= oCells.item(11).innerHTML;
					   sizeId= oCells.item(12).innerHTML;
					   itemUnitId = oCells.item(13).innerHTML;
					   itemPrice= oCells.item(6).innerHTML;
					   itemQty = oCells.item(7).innerHTML;
						
					   itemdiscounts= "";
					   gstAmounts= "";
					   totalAmount= oCells.item(8).innerHTML;
						

						 itemIds=itemIds+itemId+"###!###";
						 itemTypeIds=itemTypeIds+itemTypeId+"###!###";
						 itemCompanyIds=itemCompanyIds+itemCompanyId+"###!###";
						 sizeIds=sizeIds+sizeId+"###!###";
						 itemUnitIds=itemUnitIds+itemUnitId+"###!###";
						 itemQtys=itemQtys+itemQty+"###!###";
						 itemPrices=itemPrices+itemPrice+"###!###";

						 itemdiscounts=itemdiscounts+itemdiscount+"###!###";
						 gstAmounts=gstAmounts+gstAmount+"###!###";
						 totalAmounts=totalAmounts+totalAmount+"###!###";
					  
						 
					}

				document.customerform.itemIds.value=itemIds;
				document.customerform.itemTypeIds.value=itemTypeIds;
				document.customerform.itemCompanyIds.value=itemCompanyIds;
				document.customerform.sizeIds.value=sizeIds;
				document.customerform.itemUnitIds.value=itemUnitIds;
				document.customerform.itemQtys.value=itemQtys;
				document.customerform.itemPrices.value=itemPrices;
				document.customerform.itemdiscounts.value=itemdiscounts;
				document.customerform.gstAmounts.value=gstAmounts;
				document.customerform.totalAmounts.value=totalAmounts;
			 	
				 

			
		}
		
		

		function DeleteCustomerItem(rowlength1)
		{
		
alert("rowlength1 = "+rowlength1);
//var row = rowlength1.parentNode.parentNode;
//row.parentNode.removeChild(row);
			  //document.getElementById("customerItemTable").deleteRow(rowlength1);
		}
		
		
	</script>
</body>
</html>