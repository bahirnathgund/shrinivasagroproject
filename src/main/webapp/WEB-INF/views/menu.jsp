<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

  <div class="left side-menu">
                <div class="sidebar-inner slimscrollleft">

                    <div id="sidebar-menu">
                        <ul>

                            <li class="has_sub">
                                  <a href="Dashboard"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                            </li>

                            <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect"><i class="zmdi zmdi-blur-linear"></i><span>Administrator </span> <span class="menu-arrow"></span></a>
                                <ul>
                                
                            
                            <li>
                                <a href="ItemTypeMaster"><i class="fa fa-user fa-fw"></i> Item Type Master</a>
                            </li>
                            
                            <li>
                                <a href="ItemCompanyMaster"><i class="fa fa-home fa-fw"></i> Item Company Master</a>
                            </li>
                            
                            <li>
                                <a href="ItemMaster"><i class="fa fa-star fa-fw"></i> Item Master</a>
                            </li>
                            
                            <li>
                                <a href="ItemPriceMaster"><i class="fa fa-star fa-fw"></i> Item Price Master</a>
                            </li>
                            
                                </ul>
                            </li>

                              
                            <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect"><i class="zmdi zmdi-blur-linear"></i><span>Customer Details </span> <span class="menu-arrow"></span></a>
                                <ul>
                                  
		                            <li><a href="CustomerMaster"><span> Customer Master </span> </a></li>
		                            <!-- 
		                            <li><a href="CustomerLoanMaster"><span> Customer Loan Details </span> </a></li>
		                           -->
                                </ul>
                            </li>
    
                            <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect"><i class="zmdi zmdi-blur-linear"></i><span>Stock </span> <span class="menu-arrow"></span></a>
                                <ul>
                                  
		                            <li><a href="StockMaster"><span>Current Stock </span> </a></li>
		                            <li><a href="StockHistory"><span> Stock History</span> </a></li>
		                          
                                </ul>
                            </li>

                            <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect"><i class="zmdi zmdi-blur-linear"></i><span>Account Management</span> <span class="menu-arrow"></span></a>
                                <ul>
                                  
		                            <li><a href="VoucherMaster"><span> Voucher Master </span> </a></li>
		                          
                                </ul>
                            </li>
                            
                            
                            <li class="has_sub">
                             <a href="javascript:void(0);" class="waves-effect"><i class="zmdi zmdi-blur-linear"></i><span>Reports </span> <span class="menu-arrow"></span></a>
                                <ul>
                                  
		                            <li class="has_sub">
		                                <a href="javascript:void(0);" class="waves-effect"><span> Customer Report </span> <span class="menu-arrow"></span></a>
		                                <ul>
				                            <li> <a href="CustomerDetailsMonthlyReport" ><span> Customer Details Report </span> </a> </li>
				                            
				                             <li> <a href="CustomerReport" ><span> CustomerReport </span> </a> </li>
				                            
				                            <li> <a href="PendingCustomerReport" ><span>Pending Customer Report</span> </a> </li>
				                           
				                            <li> <a href="CustomerEMIReport" ><span> EMI Report </span> </a> </li>
				                          
				                            <li><a href="CustomerDailyPaymentReport" ><span> Daily Payment Report </span> </a> </li>
				                            
				                            <li><a href="CustomerDailyLoanReport"><span> Daily Loan Report </span> </a></li>
				                             
				                            <li><a href="TodayCollectionReport" ><span> Today Collection Report </span> </a></li>
				                             
				                            <li><a href="CustomerMonthlyReport" ><span> Monthly Report </span> </a></li>
				                            
				                            <li><a href="CustomerLoanClearanceReport"><span> Loan Clearance Report </span> </a></li>
				                            
		                                </ul>
		                            </li>
                                    
		                            <li class="has_sub">
		                                <a href="javascript:void(0);" class="waves-effect"><span> Bank Report </span> <span class="menu-arrow"></span></a>
		                                <ul>
				                            <li><a href="BankTransactionReport"><span> Bank Report </span> </a></li>
				                          
		                                </ul>
		                            </li>
                                  
		                          
                                </ul>
                            </li>
                           
                            <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect"><i class="zmdi zmdi-blur-linear"></i><span>User Management</span> <span class="menu-arrow"></span></a>
                                <ul>
                                  
		                            <li><a href="UserMaster"><span>User Master</span> </a></li>
		                          
                                </ul>
                            </li>
                             
                        </ul>
                        
                    </div>

                </div>

            </div>
