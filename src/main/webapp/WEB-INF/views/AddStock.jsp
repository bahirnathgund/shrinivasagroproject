<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description"
	content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
<meta name="author" content="Coderthemes">

<!-- App Favicon -->
<link rel="shortcut icon"
	href="${pageContext.request.contextPath}/resources/images/favicon.ico">

<!-- App title -->
<title>Add Stock</title>

<!-- Switchery css -->
<link
	href="${pageContext.request.contextPath}/resources/plugins/switchery/switchery.min.css"
	rel="stylesheet" />

<!-- Bootstrap CSS -->
<link
	href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css"
	rel="stylesheet" type="text/css" />

<!-- App CSS -->
<link href="${pageContext.request.contextPath}/resources/css/style.css"
	rel="stylesheet" type="text/css" />

<!-- Modernizr js -->
<script
	src="${pageContext.request.contextPath}/resources/js/modernizr.min.js"></script>


</head>


<body class="fixed-left" onLoad="init()">


	<div id="wrapper">

		<%@ include file="headerpage.jsp"%>

		<%@ include file="menu.jsp"%>

		<div class="content-page">

			<div class="content">
				<div class="container-fluid">

					<div class="row">
						<div class="col-xl-12">
							<div class="page-title-box">
								<h4 class="page-title float-left">Add Stock</h4>

								<ol class="breadcrumb float-right">
									<li class="breadcrumb-item"><a href="home">Home</a></li>
									<li class="breadcrumb-item"><a href="StockMaster">
											Stock Master</a></li>
									<li class="breadcrumb-item active">Add Stock</li>
								</ol>

								<div class="clearfix"></div>
							</div>
						</div>
					</div>

					<form name="stockform"
						action="${pageContext.request.contextPath}/AddStock"
						onSubmit="return validate()" method="post">

						<div class="row">

							<div class="col-12">

								<div class="card-box">
									
									<div class="row">	
										
										<div class="col-xl-3">
											<div class="form-group">
												<label for="itemTypeId">Item Type<span
													class="text-danger">*</span></label> <select class="form-control"
													name="itemTypeId" id="itemTypeId"  onchange="getItemCompamyAndItemList(this.value)">
													<option selected="selected" value="Default">-Select
														type-</option>

													<c:forEach var="itemTypeList" items="${itemTypeList}">
														<option value="${itemTypeList.id}">${itemTypeList.typeName}</option>
													</c:forEach>
												</select>
											</div>
											<span id="itemTypeIdSpan" style="color: #FF0000"></span>
										</div>

										<div class="col-xl-3">
											<div class="form-group">
												<label for="itemId">Item Name<span
													class="itemId">*</span></label> <select class="form-control"
													name="itemId" id="itemId"
													onchange="getItemPriceDetails(this.value)">
													<option selected="selected" value="Default">-Select
														Item-</option>
												</select>
											</div>
											<span id="itemIdSpan" style="color: #FF0000"></span>
										</div>


										<div class="col-xl-2">
											<div class="form-group">
												<label for="itemCompanyId">Company Name<span
													class="text-danger">*</span></label> <select class="form-control"
													name="itemCompanyId" id="itemCompanyId"
													onchange="getItemPriceDetails(this.value)">
													<option selected="selected" value="Default">-Select
														Company-</option>
												</select>
											</div>
											<span id="itemCompanyIdSpan" style="color: #FF0000"></span>
										</div>

										<div class="col-xl-2">
											<div class="form-group">
												<label for="sizeId">Item Size<span
													class="text-danger">*</span></label> <select class="form-control"
													name="sizeId" id="sizeId">
													<option selected="selected" value="Default">-Select
														Size-</option>

													<c:forEach var="itemSizeList" items="${itemSizeList}">
														<option value="${itemSizeList.id}">${itemSizeList.itemSize}</option>
													</c:forEach>
												</select>
											</div>
											<span id="sizeIdSpan" style="color: #FF0000"></span>
										</div>


										<div class="col-xl-2">
											<div class="form-group">
												<label for="currentStock">New Stock<span
													class="text-danger">*</span></label> <input class="form-control"
													type="text" id="currentStock" name="currentStock"
													placeholder="New Stock " style="text-transform: capitalize;">
											</div>
											<span id="currentStockSpan" style="color: #FF0000"></span>
										</div>



									</div>


									<br />
									<div class="row">
										<div class="col-xl-2"></div>
										<div class="col-xl-3">
											<a href="StockMaster"><button type="button"
													class="btn btn-secondary" value="reset" style="width: 90px">Back</button></a>
										</div>

										<div class="col-xl-3">
											<button type="reset" class="btn btn-default">Reset</button>
										</div>

										<div class="col-xl-3">
											<button class="btn btn-primary" type="submit">Submit</button>
										</div>
									</div>

								</div>

							</div>
							<!-- end col-->

						</div>

					</form>

				</div>
				<!-- container -->
			</div>

			<%@ include file="RightSidebar.jsp"%>

			<%@ include file="footer.jsp"%>

		</div>
	</div>
	<script>
		var resizefunc = [];
	</script>

	<!-- jQuery  -->
	<script
		src="${pageContext.request.contextPath}/resources/js/jquery.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/js/popper.min.js"></script>
	<!-- Tether for Bootstrap -->
	<script
		src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
	<script src="${pageContext.request.contextPath}/resources/js/detect.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/js/fastclick.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/js/jquery.blockUI.js"></script>
	<script src="${pageContext.request.contextPath}/resources/js/waves.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/js/jquery.nicescroll.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/js/jquery.scrollTo.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/js/jquery.slimscroll.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/plugins/switchery/switchery.min.js"></script>

	<script
		src="${pageContext.request.contextPath}/resources/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js"
		type="text/javascript"></script>

	<script
		src="${pageContext.request.contextPath}/resources/js/jquery.core.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/js/jquery.app.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/plugins/autoNumeric/autoNumeric.js"
		type="text/javascript"></script>

	<script>
		function clearAll() {

			$('#itemCompanyNameSpan').html('');
			$('#itemTypeIdSpan').html('');

		}

		function init() {
			clearAll();
			document.stockform.itemCompanyName.value = "";
			document.stockform.itemCompanyName.focus();
		}

		function validate() {

			clearAll();
			if (document.stockform.itemCompanyName.value == "") {
				$('#itemCompanyNameSpan').html('Please, enter  Name..!');
				document.stockform.itemCompanyName.focus();
				return false;
			} else if (document.stockform.itemCompanyName.value.match(/^[\s]+$/)) {
				$('#itemCompanyNameSpan').html('Please, enter Name..!');
				document.stockform.itemCompanyName.value = "";
				document.stockform.itemCompanyName.focus();
				return false;
			}

			if (document.stockform.itemTypeId.value == "Default") {
				$('#itemTypeIdSpan').html('Please, select Type..!');
				document.stockform.itemTypeId.focus();
				return false;
			}

		}
		

		function getItemCompamyAndItemList()
		{
			 $("#itemCompanyId").empty();
			 var itemTypeId = $('#itemTypeId').val();
			
			$.ajax({

				url : '${pageContext.request.contextPath}/getItemCompamyListByItemType',
				type : 'Post',
				data : { itemTypeId : itemTypeId},
				dataType : 'json',
				success : function(result)
						  {
								if (result) 
								{
									var option = $('<option/>');
									option.attr('value',"Default").text("-Select Company-");
									$("#itemCompanyId").append(option);
									
									for(var i=0;i<result.length;i++)
									{
										var option = $('<option />');
									    option.attr('value',result[i].id).text(result[i].itemCompanyName);
									    $("#itemCompanyId").append(option);
									 } 
								} 
								else
								{
									alert("failure111");
									//$("#ajax_div").hide();
								}

							}
				});


			 $("#itemId").empty();
			$.ajax({

				url : '${pageContext.request.contextPath}/getItemListByItemType',
				type : 'Post',
				data : { itemTypeId : itemTypeId},
				dataType : 'json',
				success : function(result)
						  {
								if (result) 
								{
									var option = $('<option/>');
									option.attr('value',"Default").text("-Select Item-");
									$("#itemId").append(option);
									
									for(var i=0;i<result.length;i++)
									{
										var option = $('<option />');
									    option.attr('value',result[i].id).text(result[i].itemName);
									    $("#itemId").append(option);
									 } 
								} 
								else
								{
									alert("failure111");
									//$("#ajax_div").hide();
								}

							}
				});
			
		}
	
		
	</script>
</body>
</html>