<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
        <meta name="author" content="Coderthemes">

        <!-- App Favicon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/images/favicon.ico">

        <!-- App title -->
        <title>Import New ChartOfAccounts</title>

        <!-- Switchery css -->
        <link href="${pageContext.request.contextPath}/resources/plugins/switchery/switchery.min.css" rel="stylesheet" />

        <!-- Bootstrap CSS -->
        <link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet" type="text/css" />

        <!-- App CSS -->
        <link href="${pageContext.request.contextPath}/resources/css/style.css" rel="stylesheet" type="text/css" />

        <!-- Modernizr js -->
        <script src="${pageContext.request.contextPath}/resources/js/modernizr.min.js"></script>


    </head>


 <body class="fixed-left" onLoad="init()">
 
	<%
		response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");//HTTP 1.1
    	response.setHeader("Pragma","no-cache"); //HTTP 1.0
    	response.setDateHeader ("Expires", 0); 
     	
    		if (session != null) 
    		{
    			if (session.getAttribute("employeeId") == null || session.getAttribute("employeeName") == null || session.getAttribute("userName") == null  || session.getAttribute("branchId") == null || session.getAttribute("branchName") == null || session.getAttribute("todayGoldRate") == null ) 
    			{
    				response.sendRedirect("login");
    			} 
    		}
	%>
	
  <div id="wrapper">

  <%@ include file="headerpage.jsp" %>

  <%@ include file="menu.jsp" %>
      
 <div class="content-page">
            
  <div class="content">
    <div class="container-fluid">

                        <div class="row">
                            <div class="col-xl-12">
                                <div class="page-title-box">
                                    <h4 class="page-title float-left">Import ChartOfAccounts</h4>

                                    <ol class="breadcrumb float-right">
                                        <li class="breadcrumb-item"><a href="home">Home</a></li>
                                        <li class="breadcrumb-item"><a href="BankMaster">Bank Master</a></li>
                                        <li class="breadcrumb-item active">Import ChartOfAccounts</li>
                                    </ol>

                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>

<form  name="chartOfAccountform" action="${pageContext.request.contextPath}/ImportNewChartOfAccount" method="post"  enctype="multipart/form-data">

		 <div class="row">
 
         		<div class="col-12">
         		
                <div class="card-box">
                
					 <div class="row">
					 
                      <div class="col-xl-4">
                      
                  <span id="statusSpan" style="color:#008000"></span>
                      </div>
                      <div class="col-xl-8">
                      <div class="form-group">
								<label for="profile_img1" class="col-sm-4 control-label">Excel File</label>
								<div class="col-sm-5">
									<input type="file" accept=".xls,.xlsx,.csv" class="form-control" id="chartOfAccount_excel" name="chartOfAccount_excel" required="required"/>
									<label id="profile_img1-error"></label>
								</div>
                     
                      </div>
					 </div>
					
                    </div>
                   
					 <div class="row">
					 	     
                      <div class="col-xl-3">
                      
                      </div>
                      
                      <div class="col-xl-3">
                      	<a href="ChartOfAccountMaster"><button type="button" class="btn btn-secondary" value="reset" style="width:90px">Back</button></a>
					 </div>
					    
                      <div class="col-xl-3">
                      	<button class="btn btn-primary" type="submit">Submit</button>
					 </div>
					 </div> 
				
					 <div class="row">
					 
                      <div class="col-xl-8">	 
				  <div class="box-header with-border">
	                
	                  <h4>  <i class="fa fa-text-width"></i> Import Instructions</h4>
	                </div>
	                <div class="box-body">
	                  <dl class="dl-horizontal">
	                    <dt>Template</dt>
	                    <dd>Use Standard Template only for Import.
	                     You can download it from <a href="DownloadChartOfAccountTemplate">here</a>. </dd>
	                  
	                  </dl>
	                 
	                </div>
                </div>
                
                <div class="col-xl-12">
                      <div class="col-xl-12" id="progressbarId">
                      	<div class="progress progress-sm m-b-20"">
                        	<div id="progressBar" class="progress-bar progress-bar-striped bg-success" role="progressbar" style="width: 0%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                      </div>
                      
                      <div  id="remainingCOAList">
                      <div class="col-xl-12">
                                <div class="card-box table-responsive" >
                                    <h4 class="m-t-0 header-title">Given COA are not added</h4>

                                    <table id="COAList" class="table table-striped table-bordered table mb-0 table-sm" cellspacing="0" width="100%">
                                        <thead>
                                        <tr>
                                            <th>COA Name</th>
                                            <th>COA No</th>
                    						<th>Status</th>
                                        </tr>
                                        </thead>

                                        <tbody>
                                 
						                  <c:forEach items="${chartofaccountList1}" var="chartofaccountList1" varStatus="loopStatus">
						                      <tr class="${loopStatus.index % 2 == 0 ? 'even' : 'odd'}">
						                        <td>${chartofaccountList1.chartOfAccountName} </td>
						                        <td>${chartofaccountList1.chartOfAccountNumber}</td>
						                        <td>${chartofaccountList1.status}</td>
						                      </tr>
										   </c:forEach>
                                        
                                        </tbody>
                                    </table>
                                </div>
                                </div>
                               
                </div>           
                </div>
                
                 <input type="hidden" id="statusFlag" name="statusFlag" value="${statusFlag}">
                 <input type="hidden" id="progressBarPer" name="progressBarPer" value="${progressBarPer}">
					 </div>						 
                    
                  <br/>
				</div>
				
                </div><!-- end col-->

         </div>

</form>

 </div> <!-- container -->
 </div>
          
    <%@ include file="RightSidebar.jsp" %>
 	
 	<%@ include file="footer.jsp" %>

 </div>
</div>
        <script>
            var resizefunc = [];
        </script>

        <!-- jQuery  -->
        <script src="${pageContext.request.contextPath}/resources/js/jquery.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/popper.min.js"></script><!-- Tether for Bootstrap -->
        <script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/detect.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/fastclick.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/jquery.blockUI.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/waves.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/jquery.nicescroll.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/jquery.scrollTo.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/jquery.slimscroll.js"></script>
        <script src="${pageContext.request.contextPath}/resources/plugins/switchery/switchery.min.js"></script>

        <script src="${pageContext.request.contextPath}/resources/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js" type="text/javascript"></script>
     
        <script src="${pageContext.request.contextPath}/resources/js/jquery.core.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/jquery.app.js"></script>
  		<script src="${pageContext.request.contextPath}/resources/plugins/autoNumeric/autoNumeric.js" type="text/javascript"></script>

<script type="text/javascript" src="http://code.jquery.com/jquery-latest.js"></script>
    <script type="text/javascript"> 
      $(document).ready( function() {
        $('#statusSpan').delay(2000).fadeOut();
      });
    </script>
<script>

function init()
{
	
	var progressBarPer = Number($('#progressBarPer').val());
    var elem = document.getElementById("progressBar"); 
    var width = 1;
    var id = setInterval(frame, 10);
    function frame() {
        if (width >= progressBarPer) {
            clearInterval(id);
        } else {
            width++; 
            elem.style.width = width + '%'; 
        }
    }
	 var statusFlag = Number($('#statusFlag').val());
	if(statusFlag==0)
		{

		document.getElementById("remainingCOAList").style.display = "none";
		document.getElementById("progressbarId").style.display = "none";
		
		}
	else if(statusFlag==1)
		{
		$('#statusSpan').html('Same record Not added..!');
		document.getElementById("remainingCOAList").style.display = "block";
		document.getElementById("progressbarId").style.display = "block";
		}
	else if(statusFlag==2)
		{
		$('#statusSpan').html('Record added successfully..!');
		document.getElementById("remainingCOAList").style.display = "none";
		document.getElementById("progressbarId").style.display = "block";
		}
	
}

</script>
    </body>
</html>