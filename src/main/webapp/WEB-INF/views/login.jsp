<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description"
	content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
<meta name="author" content="Coderthemes">

<!-- App Favicon -->
<link rel="shortcut icon"
	href="${pageContext.request.contextPath}/resources/images/favicon.ico">

<!-- App title -->
<title>Shrinivas Agro Login</title>

<!-- Bootstrap CSS -->
<link
	href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css"
	rel="stylesheet" type="text/css" />

<!-- App CSS -->
<link href="${pageContext.request.contextPath}/resources/css/style.css"
	rel="stylesheet" type="text/css" />

<!-- Modernizr js -->
<script
	src="${pageContext.request.contextPath}/resources/js/modernizr.min.js"></script>

<SCRIPT type="text/javascript">
	window.history.forward();
	function noBack() { window.history.forward(); }
</SCRIPT>

<!-- 
 <script language="JavaScript" type="text/javascript">

javascript:window.history.forward(1);

</script>
 -->
</head>

<body onLoad="init()" onload="noBack();"
	onpageshow="if (event.persisted) noBack();" onunload="">

	<div class="account-pages"></div>
	<div class="clearfix"></div>
	<div class="wrapper-page">

		<div class="account-bg">
			<div class="card-box mb-0">
				<div class="text-center m-t-20">
					<a href="index.html" class="logo"> <i
						class="zmdi zmdi-group-work icon-c-logo"></i> <span>Shrinivas Agro</span>
					</a>
				</div>
				<div class="m-t-10 p-20">
					<div class="row">
						<div class="col-12 text-center">
							<h6 class="text-muted text-uppercase m-b-0 m-t-0">Sign In</h6>
						</div>
					</div>
					<form class="m-t-20" name="loginform"
						action="${pageContext.request.contextPath}/login"
						onSubmit="return validate()" method="POST">

						<div class="form-group row">
							<div class="col-12">
								<input class="form-control" type="text" required=""
									id="username" name="username" placeholder="Username">
							</div>
						</div>

						<div class="form-group row">
							<div class="col-12">
								<input class="form-control" type="password" id="password"
									name="password" required="" placeholder="Password">
							</div>
						</div>
						<br />

						<div class="form-group text-center row m-t-10">
							<div class="col-12">
								<button
									class="btn btn-success btn-block waves-effect waves-light"
									type="submit">Log In</button>
							</div>
						</div>

						<div class="form-group row m-t-30 mb-0">
							<div class="col-12">
								<!--   <a href="pages-recoverpw.html" class="text-muted"><i class="fa fa-lock m-r-5"></i> Forgot your password?</a> -->
							</div>
						</div>

						<div class="form-group row m-t-30 mb-0">
							<div class="col-12 text-center">
								<!--  <h5 class="text-muted"><b>Sign in with</b></h5> -->
							</div>
						</div>

						<div class="form-group row mb-0 text-center">
							<div class="col-12">
								<!--    <button type="button" class="btn btn-facebook waves-effect font-14 waves-light m-t-20">
                                       <i class="fa fa-facebook m-r-5"></i> Facebook
                                    </button>

                                    <button type="button" class="btn btn-twitter waves-effect font-14 waves-light m-t-20">
                                       <i class="fa fa-twitter m-r-5"></i> Twitter
                                    </button>

                                    <button type="button" class="btn btn-googleplus waves-effect font-14 waves-light m-t-20">
                                       <i class="fa fa-google-plus m-r-5"></i> Google+
                                    </button> -->
							</div>
						</div>

					</form>

				</div>

				<div class="clearfix"></div>
			</div>
		</div>
		<!-- end card-box-->

		<div class="m-t-20">
			<div class="text-center">
				<!--  <p class="text-white">Don't have an account? <a href="pages-register.html" class="text-white m-l-5"><b>Sign Up</b></a></p> -->
			</div>
		</div>

	</div>
	<!-- end wrapper page -->


	<script>
            var resizefunc = [];
        </script>

	<!-- jQuery  -->
	<script
		src="${pageContext.request.contextPath}/resources/js/jquery.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/js/popper.min.js"></script>
	<!-- Tether for Bootstrap -->
	<script
		src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
	<script src="${pageContext.request.contextPath}/resources/js/detect.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/js/fastclick.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/js/jquery.blockUI.js"></script>
	<script src="${pageContext.request.contextPath}/resources/js/waves.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/js/jquery.nicescroll.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/js/jquery.scrollTo.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/js/jquery.slimscroll.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/plugins/switchery/switchery.min.js"></script>

	<!-- App js -->
	<script
		src="${pageContext.request.contextPath}/resources/js/jquery.core.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/js/jquery.app.js"></script>

	<script>

function validate()
{
	$('#branchIdSpan').html('');
	if(document.loginform.branchId.value=="Default")
	{
		$('#branchIdSpan').html('Please, select Branch Name..!');
		document.loginform.branchId.focus();
		return false;
	}
}

function init()
{
	
	document.loginform.branchId.focus();
	/* window.onload = function () { history.forward(); }; //force user to redirect
	window.onunload = function() {};  */	
}

</script>

</body>
</html>