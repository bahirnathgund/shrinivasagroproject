<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description"
	content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
<meta name="author" content="Coderthemes">

<!-- App Favicon -->
<link rel="shortcut icon"
	href="${pageContext.request.contextPath}/resources/images/favicon.ico">

<!-- App title -->
<title>Add Item Company</title>

<!-- Switchery css -->
<link
	href="${pageContext.request.contextPath}/resources/plugins/switchery/switchery.min.css"
	rel="stylesheet" />

<!-- Bootstrap CSS -->
<link
	href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css"
	rel="stylesheet" type="text/css" />

<!-- App CSS -->
<link href="${pageContext.request.contextPath}/resources/css/style.css"
	rel="stylesheet" type="text/css" />

<!-- Modernizr js -->
<script
	src="${pageContext.request.contextPath}/resources/js/modernizr.min.js"></script>


</head>


<body class="fixed-left" onLoad="init()">


	<div id="wrapper">

		<%@ include file="headerpage.jsp"%>

		<%@ include file="menu.jsp"%>

		<div class="content-page">

			<div class="content">
				<div class="container-fluid">

					<div class="row">
						<div class="col-xl-12">
							<div class="page-title-box">
								<h4 class="page-title float-left">Add Item company</h4>

								<ol class="breadcrumb float-right">
									<li class="breadcrumb-item"><a href="home">Home</a></li>
									<li class="breadcrumb-item"><a href="BankMaster">Item
											company Master</a></li>
									<li class="breadcrumb-item active">Add Item company</li>
								</ol>

								<div class="clearfix"></div>
							</div>
						</div>
					</div>

					<form name="itemform"
						action="${pageContext.request.contextPath}/AddItemCompany"
						onSubmit="return validate()" method="post">

						<div class="row">

							<div class="col-12">

								<div class="card-box">


									<div class="row">

										<div class="col-xl-3">
											<div class="form-group">
												<label for="itemName">Company Name<span
													class="text-danger">*</span></label> <input class="form-control"
													type="text" id="itemCompanyName" name="itemCompanyName"
													placeholder="Company Name"
													style="text-transform: capitalize;">
											</div>
											<span id="itemCompanyNameSpan" style="color: #FF0000"></span>
										</div>


										<div class="col-xl-3">
											<div class="form-group">
												<label for="itemTypeId">Item Type<span
													class="text-danger">*</span></label> <select class="form-control"
													name="itemTypeId" id="itemTypeId">
													<option selected="selected" value="Default">-Select
														type-</option>

													<c:forEach var="itemTypeList" items="${itemTypeList}">
														<option value="${itemTypeList.id}">${itemTypeList.typeName}</option>
													</c:forEach>
												</select>
											</div>
											<span id="itemTypeIdSpan" style="color: #FF0000"></span>
										</div>


									</div>



									<br />
									<div class="row">
										<div class="col-xl-2"></div>
										<div class="col-xl-3">
											<a href="ItemCompanyMaster"><button type="button"
													class="btn btn-secondary" value="reset" style="width: 90px">Back</button></a>
										</div>

										<div class="col-xl-3">
											<button type="reset" class="btn btn-default">Reset</button>
										</div>

										<div class="col-xl-3">
											<button class="btn btn-primary" type="submit">Submit</button>
										</div>
									</div>

								</div>

							</div>
							<!-- end col-->

						</div>

					</form>

				</div>
				<!-- container -->
			</div>

			<%@ include file="RightSidebar.jsp"%>

			<%@ include file="footer.jsp"%>

		</div>
	</div>
	<script>
		var resizefunc = [];
	</script>

	<!-- jQuery  -->
	<script
		src="${pageContext.request.contextPath}/resources/js/jquery.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/js/popper.min.js"></script>
	<!-- Tether for Bootstrap -->
	<script
		src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
	<script src="${pageContext.request.contextPath}/resources/js/detect.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/js/fastclick.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/js/jquery.blockUI.js"></script>
	<script src="${pageContext.request.contextPath}/resources/js/waves.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/js/jquery.nicescroll.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/js/jquery.scrollTo.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/js/jquery.slimscroll.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/plugins/switchery/switchery.min.js"></script>

	<script
		src="${pageContext.request.contextPath}/resources/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js"
		type="text/javascript"></script>

	<script
		src="${pageContext.request.contextPath}/resources/js/jquery.core.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/js/jquery.app.js"></script>
	<script
		src="${pageContext.request.contextPath}/resources/plugins/autoNumeric/autoNumeric.js"
		type="text/javascript"></script>

	<script>
		function clearAll() {

			$('#itemCompanyNameSpan').html('');
			$('#itemTypeIdSpan').html('');

		}

		function init() {
			clearAll();
			document.itemform.itemCompanyName.value = "";
			document.itemform.itemCompanyName.focus();
		}

		function validate() {

			clearAll();
			if (document.itemform.itemCompanyName.value == "") {
				$('#itemCompanyNameSpan').html('Please, enter  Name..!');
				document.itemform.itemCompanyName.focus();
				return false;
			} else if (document.itemform.itemCompanyName.value.match(/^[\s]+$/)) {
				$('#itemCompanyNameSpan').html('Please, enter Name..!');
				document.itemform.itemCompanyName.value = "";
				document.itemform.itemCompanyName.focus();
				return false;
			}

			if (document.itemform.itemTypeId.value == "Default") {
				$('#itemTypeIdSpan').html('Please, select Type..!');
				document.itemform.itemTypeId.focus();
				return false;
			}

		}
	</script>
</body>
</html>